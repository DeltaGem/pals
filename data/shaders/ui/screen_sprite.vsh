// SPDX-License-Identifier: MIT
uniform mat3x2 uTransform;

in vec2 aPosition;

out vec2 vTexCoord;

void main() {
    vTexCoord = vec2(aPosition.x, 1. - aPosition.y);
    gl_Position = vec4(uTransform * vec3(aPosition.x, aPosition.y, 1.0), 0., 1.);
}
