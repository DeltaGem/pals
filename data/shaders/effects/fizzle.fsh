// SPDX-License-Identifier: MIT
#include "../math.glsli"
uniform vec4 uColor, uColor2;

out vec4 fColor;

in vec2 vSpritePos;
flat in float vAlpha;

void main() {
    fColor = mix(uColor, uColor2, abs(sin(vSpritePos.x * 4.0)));
    fColor.a *= vAlpha * saturate(1 - length(vSpritePos));
}
