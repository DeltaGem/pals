// SPDX-License-Identifier: MIT
#include "./lighting.glsli"

uniform vec3 uCamPos;
uniform samplerBuffer uFaceColors;

in vec3 vWorldPos;
in float vAlpha;

out vec4 fColor;

void main() {
    vec3 normal = normalize(cross(dFdx(vWorldPos), dFdy(vWorldPos)));
    fColor = vec4(computeLighting(
        vWorldPos, uCamPos, normal,
        texelFetch(uFaceColors, gl_PrimitiveID).rgb, vec3(1, .95, 0)
    ), vAlpha);
}
