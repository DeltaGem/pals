// SPDX-License-Identifier: MIT
in vec4 iModel_row0, iModel_row1, iModel_row2;

mat4x3 uModel = transpose(mat3x4(iModel_row0, iModel_row1, iModel_row2));

void init_wo() {}
