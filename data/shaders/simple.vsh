// SPDX-License-Identifier: MIT
uniform mat4 uViewProj;
uniform mat4x3 uModel;

in vec3 aPosition;

out vec3 vWorldPos;
invariant gl_Position;

void main() {
    vWorldPos = uModel * vec4(aPosition, 1);
    gl_Position = uViewProj * vec4(vWorldPos, 1);
}
