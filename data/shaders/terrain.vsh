// SPDX-License-Identifier: MIT
uniform uint uRows;
uniform float uSegmentSize;
uniform mat4x3 uModel;
uniform mat4 uViewProj;
uniform float uMinAlpha;
uniform float uTerrainRadius;
uniform vec4 uClippingPlane;

in float aHeight;

out vec3 vWorldPos;
out float vAlpha;
invariant gl_Position;

void main() {
    uint vid = uint(gl_VertexID), x = vid /  uRows, z = vid % uRows;
    vec2 horizontal = vec2(float(x), float(z)) * uSegmentSize;
    vec3 vModelPos = vec3(horizontal.x, aHeight, horizontal.y);

    float minDistanceFromBorder = min(uTerrainRadius - abs(vModelPos.x - uTerrainRadius), uTerrainRadius - abs(vModelPos.z - uTerrainRadius));
    // Fade out terrain at borders to merge with skybox if uMinAlpha is 0.
    vAlpha = max(uMinAlpha, clamp(minDistanceFromBorder / (uTerrainRadius * 0.35), 0.0, 1.0));
    vWorldPos = uModel * vec4(vModelPos, 1.0);
    gl_Position = uViewProj * vec4(vWorldPos, 1.0);
    gl_ClipDistance[0] = dot(vWorldPos, uClippingPlane.xyz) + uClippingPlane.w;
}
