// SPDX-License-Identifier: MIT
#include "bonematrix.glsli"

uniform mat4 uViewProj;
uniform sampler2D uTexAlbedo;
uniform mat4x3 uModel;

uniform vec4 uClippingPlane;

in vec3 aPosition;
in vec2 aTexCoord;
in uvec4 aBones;
in vec4 aWeights;

out vec3 vWorldPos;
flat out vec3 vAlbedo;
invariant gl_Position;

void main() {
    vAlbedo = texture(uTexAlbedo, aTexCoord).rgb;

    mat4x3 skinMat =
        aWeights.x * boneMatrix(aBones.x) + aWeights.y * boneMatrix(aBones.y)
        + aWeights.z * boneMatrix(aBones.z) + aWeights.w * boneMatrix(aBones.w);
    vec3 deformedPosition = skinMat * vec4(aPosition, 1.);

    vWorldPos = uModel * vec4(deformedPosition, 1.0);
    gl_ClipDistance[0] = dot(vWorldPos, uClippingPlane.xyz) + uClippingPlane.w;
    gl_Position = uViewProj * vec4(vWorldPos, 1);
}
