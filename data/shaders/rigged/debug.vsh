// SPDX-License-Identifier: MIT
#include "bonematrix.glsli"
uniform mat4 uViewProj;
uniform sampler2D uTexAlbedo;
uniform mat4x3 uModel;
uniform float uSize;

uniform vec4 uClippingPlane;

in vec3 aPosition;
in vec3 aColor;

out vec3 vWorldPos;
flat out vec3 vAlbedo;
invariant gl_Position;

void main() {
    vAlbedo = aColor;
    vWorldPos = boneMatrix(uint(gl_InstanceID)) * vec4(uSize * aPosition, 1.);
    gl_ClipDistance[0] = dot(vWorldPos, uClippingPlane.xyz) + uClippingPlane.w;
    gl_Position = uViewProj * vec4(uModel * vec4(vWorldPos, 1.), 1.);
}
