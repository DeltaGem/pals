// SPDX-License-Identifier: MIT
#include "../lighting.glsli"
uniform vec3 uCamPos;

in vec3 vWorldPos;
flat in vec3 vNormal;
flat in vec4 vAlbedo;

out vec4 fColor;

void main() {
    fColor = vec4(computeLighting(
        vWorldPos, uCamPos, vNormal,
        vAlbedo.rgb, vec3(1, .95, 0)
    ), 1);
}
