// SPDX-License-Identifier: MIT
#include "lighting.glsli"

uniform vec3 uCamPos;
uniform vec3 uAlbedo;
uniform vec3 uARM;
uniform vec3 uEmission;

in vec3 vWorldPos;

out vec4 fColor;

void main() {
    vec3 normal = normalize(cross(dFdx(vWorldPos), dFdy(vWorldPos)));
    fColor = vec4(computeLighting(
        vWorldPos, uCamPos, normal,
        uAlbedo, uARM
    ) + uEmission, 1);
}
