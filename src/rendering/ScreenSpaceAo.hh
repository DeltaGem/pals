// SPDX-License-Identifier: MIT
#pragma once
#include <vector>

#include <typed-geometry/tg-lean.hh>
#include <glow/fwd.hh>

#include <fwd.hh>

class ScreenSpaceAo {
    glow::SharedProgram mShaderSsao;
    glow::SharedProgram mShaderSsaoBlurred;
    std::vector<tg::vec3> ssaoKernel;
    std::vector<tg::vec3> ssaoNoise;
    glow::SharedTexture1D ssaoKernelTex;
    glow::SharedTexture2D ssaoNoiseTex;


    glow::SharedFramebuffer mSampleFB, mBlurFB;
    glow::SharedTextureRectangle mSampleTex;

    float mBias = .002f;
    float mRadius = .5f;
    bool mEnabled = true;

public:
    glow::SharedTextureRectangle aoTexture;

    void init();
    void resize(int w, int h);

    void render(glow::SharedTextureRectangle const&depth, tg::mat4 const&proj) const;
    void updateUI();
};
