// SPDX-License-Identifier: MIT
#pragma once
#include <typed-geometry/tg-lean.hh>
#include <glow/fwd.hh>

struct RenderTargets {
    glow::SharedFramebuffer shadowFB, depthFB, sceneFB;
    glow::SharedTextureRectangle shadowTex, depthTex, colorTex;

    int shadowSize = 4096;
    tg::color3 mBackgroundColor {1, 1, 1};

    void init();
    void resize(int w, int h);
    void resizeShadow();
    void updateUI();
};
