// SPDX-License-Identifier: MIT
#pragma once
#include <glow/fwd.hh>
#include <typed-geometry/tg-lean.hh>

#include <fwd.hh>
#include <environment/Wind.hh>

namespace Render {

struct DepthPass {
    ECS::Snapshot *snap;
    double wallTime;

    tg::mat4x3 viewMatrix;
    tg::mat4 projMatrix;
    tg::mat4 viewProjMatrix;
    tg::vec4 clippingPlane;
    tg::isize2 viewPortSize;

    Wind::Uniforms windUniforms;

    void applyTime(glow::UsedProgram& shader) const;
    void applyCommons(glow::UsedProgram& shader) const;
    void applyWind(glow::UsedProgram& shader) const;
};

struct MainPass : public DepthPass {
    tg::pos3 cameraPosition;

    glow::SharedTextureRectangle shadowTex;
    glow::SharedTextureRectangle ssaoTex;
    glow::SharedUniformBuffer lightingUniforms;

    MainPass(DepthPass const&pass) : DepthPass(pass) {}
    MainPass(DepthPass &&pass) : DepthPass(pass) {}
    void applyCommons(glow::UsedProgram& shader) const;
};

}
