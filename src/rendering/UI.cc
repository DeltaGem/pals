// SPDX-License-Identifier: MIT
#include <glow/objects/TextureRectangle.hh>

#include "GaussianBlur.hh"
#include "PostProcess.hh"
#include "RenderTargets.hh"
#include "ScreenSpaceAo.hh"
#include <util/ImGui.hh>

void GaussianBlur::updateUI() {
    bool update = false;
    update |= ImGui::SliderInt("Kernel Size", &mKernelSize, 1, 30);
    update |= ImGui::SliderFloat("StdDev", &mKernelSigma, 1, 10);
    if (update) {updateKernel();}
}

void RenderTargets::updateUI() {
    ImGui::ColorEdit3("Background", &mBackgroundColor.r);
    if (ImGui::InputInt("Shadowmap size", &shadowSize)) {resizeShadow();}
}

void PostProcess::updateUI() {
    if (ImGui::TreeNode("Vignette")) {
        float vignetteBorder[2] {mVignetteBorder.x, mVignetteBorder.y};
        if (ImGui::SliderFloat2("border", vignetteBorder, 0.f, 1.f)) {
            mVignetteBorder = {vignetteBorder[0], vignetteBorder[1]};
        }
        Util::editColor("paused color", mVignettePaused);
        Util::editColor("warning color", mVignetteWarning);
        ImGui::TreePop();
    }
    if (ImGui::TreeNodeEx("Depth of Field", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::PushItemWidth(ImGui::GetWindowWidth() * .2f);
        ImGui::Checkbox("Enable", &mEnableDoF); ImGui::SameLine();
        ImGui::InputFloat("Near", &mStartDoFDistance); ImGui::SameLine();
        ImGui::InputFloat("Far", &mFinalDoFDistance);
        ImGui::PopItemWidth();
        ImGui::InputFloat("FocusDistance", &mFocusDistance);
        mGaussianBlur.updateUI();
        ImGui::TreePop();
    }
}

void ScreenSpaceAo::updateUI() {
    ImGui::Checkbox("Enabled", &mEnabled);
    ImGui::InputFloat("Radius", &mRadius, 0, 1);
    ImGui::InputFloat("Bias", &mBias, 0, 1);
}
