// SPDX-License-Identifier: MIT
#pragma once
#include <glow/fwd.hh>

#include <Rigged.hh>
#include <ECS/Misc.hh>


/// only for *shared* shaders/programs: remove entries as soon as only one
/// system/tool uses them. (assets that are loaded from a single file and used by
/// different systems/tools are okay, even if they each have only one user)
struct SharedResources {
    struct PassShaders {
        glow::SharedProgram depth, main;
    };
    PassShaders simple, rigged, riggedDebug;
    glow::SharedTexture2D colorPaletteTex, logo;
    glow::SharedVertexArray tetrahedronMarker, spriteQuad, coordHelper;

    Rigged::Mesh parrot;
    Rigged::BoneHierarchy parrotBoneHier;
    std::unordered_map<std::string, Rigged::Animation> parrotAnimations;

    void init();
    void setCommonUniforms(const glow::SharedUniformBuffer &);
};
