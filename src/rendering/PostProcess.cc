// SPDX-License-Identifier: MIT
#include "PostProcess.hh"
#include <typed-geometry/tg.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/common/scoped_gl.hh>
#include <glow-extras/geometry/FullscreenTriangle.hh>

#include "RenderTargets.hh"

void PostProcess::init() {
    mShaderOutput = glow::Program::createFromFiles({"../data/shaders/fullscreen_tri.vsh", "../data/shaders/output.fsh"});
    mGaussianBlur.init();
    glow::geometry::FullscreenTriangle::init();
}

void PostProcess::render(RenderTargets &rt, const tg::mat4& projMat, float timePassed, bool paused) {
    auto blurredTex = mEnableDoF
        ? mGaussianBlur.blurTex(rt.colorTex)
        : rt.colorTex;

    auto vignetteColor = paused ? mVignettePaused : tg::color4(0, 0, 0, 0);
    mVignetteTime = std::max(mVignetteTime - timePassed, 0.f);
    vignetteColor = tg::lerp(vignetteColor, mVignettePulse, mVignetteTime);

    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);

    {
        auto shader = mShaderOutput->use();
        shader["uTexColor"] = rt.colorTex;
        shader["uStartDoFDistance"] = mStartDoFDistance;
        shader["uFinalDoFDistance"] = mFinalDoFDistance;
        shader["uInvProjMat"] = tg::inverse(projMat);
        shader["uBlurredTexColor"] = blurredTex;
        shader["uTexDepth"] = rt.depthTex;
        shader["uWsFocusDistance"] = mFocusDistance;
        shader["uVignetteBorder"] = mVignetteBorder;
        shader["uVignetteColor"] = vignetteColor;
        shader["uFade"] = fade;

        glow::geometry::FullscreenTriangle::draw();
    }
}

void PostProcess::resize(int w, int h) {
    mGaussianBlur.resize(w, h);
}

void PostProcess::flashWarning(float fadeTime) {
    mVignettePulse = mVignetteWarning;
    mVignetteTime = fadeTime;
}
