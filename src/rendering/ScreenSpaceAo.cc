// SPDX-License-Identifier: MIT
#include "ScreenSpaceAo.hh"
#include <random>

#include <glow/common/scoped_gl.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture1D.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow-extras/geometry/FullscreenTriangle.hh>

#include <ECS.hh>
#include <rendering/RenderPass.hh>


void ScreenSpaceAo::render(glow::SharedTextureRectangle const&depth, tg::mat4 const&proj) const {
    if (!mEnabled) {
        mBlurFB->bind().clearColor("ao", 1.f);
        return;
    }
    {
        auto fb = mSampleFB->bind();
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        auto shader = mShaderSsao->use();
        shader["uDepthTex"] = depth;
        shader["uProjMat"] = proj;
        shader["uInvProjMat"] = tg::inverse(proj);
        shader["uSsaoKernelTex"] = ssaoKernelTex;
        shader["uNoiseTex"] = ssaoNoiseTex;
        shader["uBias"] = mBias;
        shader["uRadius"] = mRadius;

        glow::geometry::FullscreenTriangle::draw();
    } {
        auto fb = mBlurFB->bind();
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        auto shader = mShaderSsaoBlurred->use();
        shader["ssaoInput"] = mSampleTex;

        glow::geometry::FullscreenTriangle::draw();
    }
}

void ScreenSpaceAo::init() {
    std::uniform_real_distribution<float> randomFloats(0.0, 1.0); // random floats between [0.0, 1.0]
    std::default_random_engine generator;

    auto lerp = [&](float a, float b, float f) {
      return a + f * (b - a);
    };

    for (unsigned int i = 0; i < 16; ++i)
    {
        tg::vec3 sample(
            randomFloats(generator) * 2.0 - 1.0,
            randomFloats(generator) * 2.0 - 1.0,
            randomFloats(generator)
        );
        sample  = tg::normalize(sample);
        sample *= randomFloats(generator);
        float scale = (float)i / 16.0f;
        scale   = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel.push_back(sample);
    }

    ssaoKernelTex = glow::Texture1D::create(16, GL_RGB16F);
    {
        auto boundTex = ssaoKernelTex->bind();
        boundTex.setFilter(GL_NEAREST, GL_NEAREST);
        boundTex.setData(GL_RGB16F, 16,  ssaoKernel);
    }

    for (unsigned int i = 0; i < 16; i++)
    {
        tg::vec3 noise(
            randomFloats(generator) * 2.0 - 1.0,
            randomFloats(generator) * 2.0 - 1.0,
            0.0f);
        ssaoNoise.push_back(noise);
    }

    ssaoNoiseTex = glow::Texture2D::create(4, 4, GL_RGBA16F);
    {
        auto boundTex = ssaoNoiseTex->bind();
        boundTex.setFilter(GL_NEAREST, GL_NEAREST);
        boundTex.setWrap(GL_REPEAT, GL_REPEAT);
        boundTex.setData(GL_RGB16F, 4, 4, ssaoNoise);
    }


    mShaderSsao = glow::Program::createFromFiles({"../data/shaders/fullscreen_tri.vsh", "../data/shaders/ssao/ssao.fsh"});
    mShaderSsaoBlurred = glow::Program::createFromFiles({"../data/shaders/fullscreen_tri.vsh", "../data/shaders/ssao/ssaoblur.fsh"});

    mSampleTex = glow::TextureRectangle::create(1, 1, GL_R16F);
    {
        auto boundTex = mSampleTex->bind();
        boundTex.setMinFilter(GL_NEAREST);
        boundTex.setMagFilter(GL_NEAREST);
    }

    mSampleFB = glow::Framebuffer::create();
    {
        auto boundFb = mSampleFB->bind();
        boundFb.attachColor("ao", mSampleTex);
        boundFb.checkComplete();
    }

    aoTexture = glow::TextureRectangle::create(1, 1, GL_R16F);
    {
        auto boundTex = aoTexture->bind();
        boundTex.setMinFilter(GL_NEAREST);
        boundTex.setMagFilter(GL_NEAREST);
    }

    mBlurFB = glow::Framebuffer::create();
    {
        auto boundFb = mBlurFB->bind();
        boundFb.attachColor("ao", aoTexture);

        boundFb.checkComplete();
    }

}

void ScreenSpaceAo::resize(int w, int h) {
    for (auto &t : {
        mSampleTex, aoTexture
    }) {
        t->bind().resize(w, h);
    }
}
