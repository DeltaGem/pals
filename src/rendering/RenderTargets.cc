// SPDX-License-Identifier: MIT
#include "RenderTargets.hh"
#include <algorithm>

#include <glow-extras/colors/color.hh>
#include <glow/glow.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/TextureRectangle.hh>
#include <typed-geometry/tg.hh>

void RenderTargets::init() {
        // size is 1x1 for now and is changed onResize
        colorTex = glow::TextureRectangle::create(1, 1, GL_R11F_G11F_B10F);
        depthTex = glow::TextureRectangle::create(1, 1, GL_DEPTH_COMPONENT32F);

        // create framebuffer for HDR scene
        depthFB = glow::Framebuffer::create(); {
            auto boundFb = depthFB->bind();
            boundFb.attachDepth(depthTex);
            boundFb.checkComplete();
        }
        sceneFB = glow::Framebuffer::create("fColor", colorTex, depthTex);

        shadowTex = glow::TextureRectangle::create(shadowSize, shadowSize, GL_DEPTH_COMPONENT32F); {
            auto boundTex = shadowTex->bind();
            boundTex.setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
            boundTex.setCompareFunc(GL_LEQUAL);
            boundTex.setAnisotropicFiltering(1);
            boundTex.setWrap(GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER);
            boundTex.setBorderColor(tg::color(1.f, 1.f, 1.f, 1.f));
        }

        shadowFB = glow::Framebuffer::create(); {
            auto boundFb = shadowFB->bind();
            boundFb.attachDepth(shadowTex);
            boundFb.checkComplete();
        }
}

void RenderTargets::resize(int w, int h) {
    for (auto &t : {
        colorTex, depthTex
    }) {
        t->bind().resize(w, h);
    }
}

void RenderTargets::resizeShadow() {
    shadowSize = std::clamp(shadowSize, 1, 8192);
    shadowTex->bind().resize(shadowSize, shadowSize);
}
