// SPDX-License-Identifier: MIT
#include "RenderPass.hh"
#include <glow/objects.hh>
#include <ECS.hh>

using namespace Render;

void DepthPass::applyCommons(glow::UsedProgram& shader) const {
    shader["uViewProj"] = viewProjMatrix;
    shader["uClippingPlane"] = clippingPlane;
}
void DepthPass::applyTime(glow::UsedProgram& shader) const {
    shader["uTime"] = float(snap->worldTime);
}
void DepthPass::applyWind(glow::UsedProgram& shader) const {
    shader["uWindSettings"] = windUniforms.windSettings;
    shader["uWindTex"] = windUniforms.windTexture;
}

void MainPass::applyCommons(glow::UsedProgram& shader) const {
    DepthPass::applyCommons(shader);
    shader["uCamPos"] = cameraPosition;
    shader["uSsaoTex"] = ssaoTex;
    shader["uSunShadowTex"] = shadowTex;
}
