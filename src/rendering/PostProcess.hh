// SPDX-License-Identifier: MIT
#pragma once
#include <typed-geometry/tg-lean.hh>
#include <glow/fwd.hh>

#include "GaussianBlur.hh"

struct RenderTargets;

class PostProcess {
private:
    glow::SharedProgram mShaderOutput;
    GaussianBlur mGaussianBlur;

    float mStartDoFDistance = 10;
    float mFinalDoFDistance = 40;

    bool mEnableDoF = true;
    tg::vec2 mVignetteBorder {.8f, .8f};
    tg::color4 mVignettePaused {0, 0, 0, .5f};
    tg::color4 mVignetteWarning {1, 0, 0, .5f};
    float mVignetteTime = 0.f;
    tg::color4 mVignettePulse;

public:
    float mFocusDistance = 20;
    float fade = 1.f;

    void init();
    void render(RenderTargets &, const tg::mat4& projMat, float timePassed, bool paused);
    void updateUI();
    void resize(int w, int h);

    void flashWarning(float fadeTime);
};
