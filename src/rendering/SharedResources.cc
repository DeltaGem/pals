// SPDX-License-Identifier: MIT
#include "SharedResources.hh"
#include <glow/data/ColorSpace.hh>
#include <glow/objects.hh>
#include <glow-extras/geometry/Quad.hh>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <Util.hh>

void SharedResources::init() {
    colorPaletteTex = glow::Texture2D::createFromFile("../data/textures/ColorPaletteGrid.png", glow::ColorSpace::sRGB);
    logo = glow::Texture2D::createFromFile("../data/textures/Logo.png", glow::ColorSpace::Linear);

    auto simpleVSh = glow::Shader::createFromFile(GL_VERTEX_SHADER, "../data/shaders/simple.vsh"), riggedFSh = glow::Shader::createFromFile(
        GL_FRAGMENT_SHADER, "../data/shaders/rigged/rigged.fsh"
    ), riggedVSh = glow::Shader::createFromFile(
        GL_VERTEX_SHADER, "../data/shaders/rigged/rigged_mesh.vsh"
    ), riggedDebugVSh = glow::Shader::createFromFile(
        GL_VERTEX_SHADER, "../data/shaders/rigged/debug.vsh"
    );
    simple = {
        glow::Program::create(simpleVSh),
        glow::Program::create({simpleVSh, glow::Shader::createFromFile(
            GL_FRAGMENT_SHADER, "../data/shaders/simple.fsh"
        )})
    };
    rigged = {
        glow::Program::create(riggedVSh),
        glow::Program::create({riggedVSh, riggedFSh})
    };
    riggedDebug = {
        glow::Program::create(riggedDebugVSh),
        glow::Program::create({riggedDebugVSh, riggedFSh})
    };
    // Disable warnings since Program::getUniform does not work for arrays, which leads to the warning despite the data being present.
    for (auto &sh : {
        rigged.depth, rigged.main, riggedDebug.depth, riggedDebug.main
    }) {
        sh->setWarnOnUnchangedUniforms(false);
    }

    Assimp::Importer imp;
    imp.SetPropertyFloat(AI_CONFIG_PP_DB_THRESHOLD, 4.0f);
    auto scene = imp.ReadFile("../data/meshes/parrot.dae", aiProcess_SortByPType | aiProcess_Debone | aiProcess_FlipUVs);
    TG_ASSERT(scene && scene->mNumMeshes == 1);
    auto mesh = scene->mMeshes[0];
    std::unordered_map<std::string, std::uint8_t> boneMap;
    for (auto i : Util::IntRange(mesh->mNumBones)) {
        boneMap.emplace(mesh->mBones[i]->mName.C_Str(), boneMap.size());
        TG_ASSERT(boneMap.size() < 256);
    }
    boneMap.emplace("ParrotArmature", boneMap.size());
    parrotBoneHier = Rigged::boneHierarchy(scene, boneMap);
    parrot = Rigged::loadMesh(mesh, boneMap);
    glow::info() << "Animations";
    for (auto i : Util::IntRange(scene->mNumAnimations)) {
        auto anim = scene->mAnimations[i];
        glow::info() << "* " <<  anim->mName.C_Str();
        for (auto j : Util::IntRange(anim->mNumChannels)) {
            auto chan = anim->mChannels[j];
            glow::info() << "  * " << chan->mNodeName.C_Str();
        }
        parrotAnimations[anim->mName.C_Str()].load(anim, boneMap);
    }

    std::vector<tg::pos3> markerVerts;
    markerVerts.reserve(4);
    markerVerts.push_back({0, 0, 0});
    for (int i = 0; i < 3; ++i) {
        auto [sin, cos] = tg::sin_cos(tg::angle::from_degree(-120 * i));
        markerVerts.push_back({sin / 3, 1, cos / 3});
    }
    tetrahedronMarker = glow::VertexArray::create(
        glow::ArrayBuffer::create("aPosition", markerVerts),
        glow::ElementArrayBuffer::create({
            0, 1, 2,    0, 2, 3,    0, 3, 1,    3, 2, 1
        }),
        GL_TRIANGLES
    );
    struct CoordHelperVertex {tg::pos3 pos; tg::color3 color;};
    std::initializer_list<tg::pos3> prong {
        {0, 0, 0}, {1, -.1f, -.1f}, {1, -.1f, .1f}, {1, .1f, .1f}, {1, .1f, -.1f}
    };
    std::vector<CoordHelperVertex> coordHelperVertices;
    for (auto p : prong) {
        coordHelperVertices.push_back({p, {1, 0, 0}});
    }
    for (auto p : prong) {
        coordHelperVertices.push_back({{-p.y, p.x, p.z}, {0, 1, 0}});
    }
    for (auto p : prong) {
        coordHelperVertices.push_back({{-p.z, p.y, p.x}, {0, 0, 1}});
    }
    std::uint16_t END = 0xffff;
    coordHelper = glow::VertexArray::create(
        glow::ArrayBuffer::create({
            {&CoordHelperVertex::pos, "aPosition"},
            {&CoordHelperVertex::color, "aColor"}
        }, std::move(coordHelperVertices)),
        glow::ElementArrayBuffer::create(std::vector<std::uint16_t>{
            0, 1, 2, 3, 4, 1, END, 4, 3, 2, 1, END,
            5, 6, 7, 8, 9, 6, END, 9, 8, 7, 6, END,
            10, 11, 12, 13, 14, 11, END, 14, 13, 12, 11, END,
        }),
        GL_TRIANGLE_FAN
    );
    spriteQuad = glow::geometry::Quad<tg::pos2>().generate();
}

void SharedResources::setCommonUniforms(const glow::SharedUniformBuffer &lightingUniforms) {
    for (auto sh : {&simple, &rigged}) {
        sh->main->setUniformBuffer("uLighting", lightingUniforms);
    }
}
