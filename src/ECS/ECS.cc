// SPDX-License-Identifier: MIT
#include "Misc.hh"
#include <cmath>

#include <glow/common/log.hh>

#include <ECS.hh>
#include <MathUtil.hh>
#include <combat/Combat.hh>
#include <demo/Demo.hh>
#include <effects/Effects.hh>
#include <navmesh/NavMesh.hh>
#include <obstacles/Obstacle.hh>
#include <obstacles/WorldFluff.hh>
#include <rendering/MeshViz.hh>
#include <startsequence/StartSequence.hh>
#include <terrain/SkyBox.hh>
#include <terrain/Terrain.hh>
#include <terrain/Water.hh>
#include <ui/SpriteRenderer.hh>
#include <environment/Parrot.hh>

#include "SimpleMesh.hh"

ECS::entity ECS::ECS::newEntity() {
    if (freeEntities.empty()) {
        glow::info() << "allocating new entity " << nextEntity;
        return nextEntity++;
    }
    auto res = freeEntities.back();
    glow::info() << "recycling entity ID " << res;
    freeEntities.pop_back();
    return res;
}
void ECS::ECS::deleteEntity(entity id) {
    humanoids.erase(id);
    mobileUnits.erase(id);
    demoAnim.erase(id);
    staticRigids.erase(id);
    instancedRigids.erase(id);
    vizMeshes.erase(id);
    navMeshes.erase(id);
    obstacles.erase(id);
    simpleMeshes.erase(id);
    scatterLasers.erase(id);
    skyBoxes.erase(id);
    terrains.erase(id);
    terrainRenderings.erase(id);
    waters.erase(id);
    worldFluffs.erase(id);
    riggedRigids.erase(id);
    parrots.erase(id);

    freeEntities.push_back(id);
}

tg::mat4x3 ECS::Rigid::transform_mat() const {
    return Util::transformMat(translation, rotation);
}
ECS::Rigid ECS::Rigid::operator~() const {
    auto rot = tg::conjugate(rotation);
    return {tg::pos3(rot * -translation), rot};
}
ECS::Rigid ECS::Rigid::chain(const Rigid &other) const {
    auto mat = transform_mat();
    return {
        tg::pos3(mat * tg::vec4(other.translation, 1.f)),
        tg::normalize(rotation * other.rotation)
    };
}
ECS::Rigid ECS::Rigid::interpolate(const Rigid &other, float param) const {
    return {
        tg::lerp(translation, other.translation, param),
        tg::slerp(rotation, other.rotation, param)
    };
}
tg::vec3 ECS::operator*(const Rigid &a, const tg::vec3 &b) {
    return a.rotation * b;
}
tg::dir3 ECS::operator*(const Rigid &a, const tg::dir3 &b) {
    return a.rotation * b;
}
tg::pos3 ECS::operator*(const Rigid &a, const tg::pos3 &b) {
    return a.translation + a.rotation * b;
}

void ECS::ECS::init(Game &game) {
    combatSys = std::make_unique<Combat::System>(game);
    demoSys = std::make_unique<Demo::System>(*this);
    effectsSys = std::make_unique<Effects::System>(game);
    navMeshSys = std::make_unique<NavMesh::System>(*this);
    meshVizSys = std::make_unique<MeshViz::System>(*this);
    obstacleRenderer = std::make_unique<Obstacle::Renderer>(game);
    fluffSys = std::make_unique<Obstacle::FluffSystem>(game);
    obstacleSys = std::make_unique<Obstacle::System>(game);
    parrotSys = std::make_unique<Parrot::System>(game);
    skyBoxSys = std::make_unique<Terrain::SkyBoxSystem>(*this);
    terrainSys = std::make_unique<Terrain::System>(*this);
    waterSys = std::make_unique<Water::System>(*this);
    spriteSys = std::make_unique<Sprite::System>(game);
    startSequenceSys = std::make_unique<StartSequence::System>(game);
}

ECS::ECS::~ECS() {}

void ECS::Snapshot::clear() {
    humRender.clear();
    parrots.clear();
    parrotBones.clear();
    sprites.clear();
}

void ECS::ECS::update(Snapshot &prev, Snapshot &next) {
    next.clear();
    // put here: extrapolate all interacting objects (or those whose position
    // is computed by integration)
    combatSys->extrapolate(prev, next);

    // compute interactions
    combatSys->update(prev, next);
    parrotSys->update(next);
    startSequenceSys->update(next);

    // clean up: erase destroyed objects, free unneeded resources
    effectsSys->cleanup(next.worldTime);
}

void ECS::ECS::extrapolateRender(Snapshot &upd, Snapshot &render) {
    render.clear();
    // extrapolate ALL objects
    render.rigids = staticRigids;
    render.riggedRigids = riggedRigids;
    combatSys->extrapolate(upd, render);
    combatSys->prepareRender(render);
    demoSys->extrapolate(render);

    // NOTE: emits parrots, so must be before parrotSys
    startSequenceSys->prepareRender(render);

    parrotSys->prepareRender(render);
}

void ECS::ECS::renderShadow(Render::DepthPass const&pass) const {
    demoSys->renderDepth(pass);
    obstacleRenderer->renderDepth(pass);
    terrainSys->renderDepth(pass);
}

void ECS::ECS::renderDepthPre(Render::DepthPass const&pass) const {
    combatSys->renderDepth(pass);
    demoSys->renderDepth(pass);
    obstacleRenderer->renderDepth(pass);
    parrotSys->renderDepth(pass);
    startSequenceSys->renderDepth(pass);
    terrainSys->renderDepth(pass);
    waterSys->renderDepth(pass);
}

void ECS::ECS::renderReflectRefract(Render::MainPass const&pass) const {
    combatSys->renderMain(pass);
    demoSys->renderMain(pass);
    obstacleRenderer->renderReflectRefract(pass);
    skyBoxSys->renderMain(pass);
    terrainSys->renderMain(pass, 0.0f);
}

void ECS::ECS::renderMain(Render::MainPass const&pass) const {
    combatSys->renderMain(pass);
    demoSys->renderMain(pass);
    meshVizSys->renderMain(pass);
    obstacleRenderer->renderMain(pass);
    parrotSys->renderMain(pass);
    terrainSys->renderMain(pass, 1.0f);
    waterSys->renderMain(pass);
    skyBoxSys->renderMain(pass);
    startSequenceSys->renderMain(pass);
}

void ECS::ECS::renderTransparent(Render::MainPass const&pass) const {
    effectsSys->renderMain(pass);
    startSequenceSys->renderTransparent(pass);
}

void ECS::ECS::renderUI(Snapshot const& snap) const {
    spriteSys->render(snap);
}

void ECS::ECS::editorUI() {
    combatSys->editorUI();
    navMeshSys->editorUI();
    obstacleSys->editorUI();
    terrainSys->editorUI();
}

void ECS::ECS::select(tg::ray3 const& ray) {
    selectedEntity = INVALID;
    float dist = HUGE_VALF;
    combatSys->select(ray, dist);
    navMeshSys->select(ray, dist);
    obstacleSys->select(ray, dist);
}
