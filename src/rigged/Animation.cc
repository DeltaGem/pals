// SPDX-License-Identifier: MIT
#include <Rigged.hh>
#include <cmath>
#include <algorithm>

#include <typed-geometry/tg.hh>
#include <assimp/anim.h>

#include <Util.hh>
#include <ECS/Misc.hh>

using namespace Rigged;

void Animation::load(aiAnimation *anim, const std::unordered_map<std::string, std::uint8_t> &boneMap) {
    nBones = boneMap.size();
    TG_ASSERT(anim->mNumChannels == nBones);
    if (nBones == 0) {return;}
    size_t nKeyFrames = anim->mChannels[0]->mNumPositionKeys;
    TG_ASSERT(nKeyFrames > 0);
    times.reserve(nKeyFrames);
    std::vector<const aiNodeAnim *> boneAnims(nBones, nullptr);
    for (auto i : Util::IntRange(nBones)) {
        auto &ba = *anim->mChannels[i];
        auto iter = boneMap.find(ba.mNodeName.C_Str());
        TG_ASSERT(iter != boneMap.end());
        TG_ASSERT(boneAnims.at(iter->second) == nullptr);
        boneAnims.at(iter->second) = &ba;
        TG_ASSERT(nKeyFrames == ba.mNumPositionKeys);
        TG_ASSERT(nKeyFrames == ba.mNumRotationKeys);
    }
    for (auto i : Util::IntRange(nKeyFrames)) {
        double time = anim->mChannels[0]->mPositionKeys[i].mTime;
        if (!times.empty()) {
            TG_ASSERT(times.back() <= float(time));
        }
        times.push_back(float(time));
        for (auto ptr : boneAnims) {
            auto &posFr = ptr->mPositionKeys[i];
            auto &rotFr = ptr->mRotationKeys[i];
            TG_ASSERT(posFr.mTime == time);
            TG_ASSERT(rotFr.mTime == time);
            pos.emplace_back(tg::pos3(posFr.mValue), toQuat(rotFr.mValue));
        }
    }
}

void Animation::interpolate(float time, std::vector<ECS::Rigid> &out) const {
    auto iter = std::upper_bound(times.begin(), times.end(), time);
    if (iter == times.begin()) {
        std::copy_n(pos.begin(), nBones, std::back_inserter(out));
        return;
    } else if (iter == times.end()) {
        std::copy_n(pos.end() - nBones, nBones, std::back_inserter(out));
        return;
    }
    auto start = nBones * std::distance(times.begin(), iter);
    auto prevTime = *(iter - 1), nextTime = *iter;
    auto param = (time - prevTime) / (nextTime - prevTime);
    for (auto b : Util::IntRange(start, start + nBones)) {
        out.push_back(pos[b].interpolate(pos[b + nBones], param));
    }
}
