// SPDX-License-Identifier: MIT
#include <Rigged.hh>
#include <typed-geometry/tg.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/common/log.hh>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <Util.hh>
#include <ECS/Misc.hh>

using namespace Rigged;

Mesh Rigged::loadMesh(const aiMesh *mesh, const BoneMap &boneMap) {
    struct RiggedVertex {
        tg::pos3 pos;
        tg::pos2 uv;
        tg::u8vec4 bones;
        tg::vec4 weights;
    };
    TG_ASSERT(mesh->mNumUVComponents[0] == 2);
    std::vector<RiggedVertex> vertices;
    vertices.reserve(mesh->mNumVertices);
    for (auto i : Util::IntRange(mesh->mNumVertices)) {
        vertices.push_back({tg::pos3(mesh->mVertices[i]), tg::pos2(mesh->mTextureCoords[0][i]), {}, {}});
    }
    TG_ASSERT(mesh->mNumBones <= 256);
    std::vector<ECS::Rigid> invBindXForm(mesh->mNumBones);
    for (auto i : Util::IntRange(mesh->mNumBones)) {
        auto bone = mesh->mBones[i];
        glow::info() << "* " << bone->mName.C_Str();
        auto boneID = boneMap.find(bone->mName.C_Str());
        TG_ASSERT(boneID != boneMap.end() && boneID->second < invBindXForm.size());
        auto &xform = invBindXForm[boneID->second];
        aiQuaternion rot; aiVector3D pos;
        bone->mOffsetMatrix.DecomposeNoScaling(rot, pos);
        xform.rotation = toQuat(rot);
        xform.translation = tg::pos3(pos);
        for (auto j : Util::IntRange(bone->mNumWeights)) {
            auto &w = bone->mWeights[j];
            auto v = w.mVertexId;
            TG_ASSERT(v < vertices.size());
            auto &vtx = vertices[v];
            for(int b = 0; true; ++b) {
                TG_ASSERT(b < 4);
                if (vtx.weights[b] != 0.f) {continue;}
                vtx.bones[b] = boneID->second;
                vtx.weights[b] = w.mWeight;
                break;
            }
        }
    }
    auto abo = glow::ArrayBuffer::create();
    for (auto &i : std::initializer_list<glow::ArrayBufferAttribute> {
        {&RiggedVertex::pos, "aPosition"},
        {&RiggedVertex::uv, "aTexCoord"},
        {&RiggedVertex::bones, "aBones"},
        {&RiggedVertex::weights, "aWeights"}
    }) {
        abo->defineAttribute(std::move(i));
    }
    abo->bind().setData(vertices);
    TG_ASSERT(vertices.size() < 0xffff);
    using u16 = std::uint16_t;
    std::vector<u16> indices;
    for (auto i : Util::IntRange(mesh->mNumFaces)) {
        auto &face = mesh->mFaces[i];
        TG_ASSERT(face.mNumIndices >= 2);
        auto a = face.mIndices[0];
        TG_ASSERT(a < vertices.size());
        for (auto j : Util::IntRange(face.mNumIndices - 2)) {
            auto b = face.mIndices[j + 1], c = face.mIndices[j + 2];
            TG_ASSERT(b < vertices.size() && c < vertices.size());
            indices.insert(indices.end(), {u16(a), u16(b), u16(c)});
        }
    }
    return {
        glow::VertexArray::create(abo, glow::ElementArrayBuffer::create(indices)),
        std::move(invBindXForm)
    };
}

std::vector<std::pair<boneid, boneid>> Rigged::boneHierarchy(const aiScene *scene, const BoneMap &boneMap) {
    std::vector<aiNode const*> stack;
    stack.push_back(scene->mRootNode);
    std::vector<std::pair<boneid, boneid>> res;
    while (!stack.empty()) {
        aiNode const *node = stack.back(); stack.pop_back();
        for (auto i : Util::IntRange(node->mNumChildren)) {
            stack.push_back(node->mChildren[i]);
        }
        auto parentID = boneMap.find(node->mName.C_Str());
        if (parentID == boneMap.end()) {continue;}
        for (auto i : Util::IntRange(node->mNumChildren)) {
            auto childID = boneMap.find(node->mChildren[i]->mName.C_Str());
            if (childID != boneMap.end()) {
                res.push_back({parentID->second, childID->second});
                glow::info() << parentID->first << " " << childID->first;
            }
        }
    }
    return res;
}
