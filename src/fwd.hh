// SPDX-License-Identifier: MIT
#pragma once

class Game;
struct SharedResources;
struct SimpleMesh;

namespace Anim {
    template<typename T> struct Animation;
    template<typename T> struct KeyFrame;
    template<typename T> struct Animator;
};

namespace Combat {
    struct MobileUnit;
    struct Humanoid;
    struct HumanoidPos;
    struct HumanoidRenderInfo;
    class System;
}

namespace Demo {
    struct Animation;
    class System;
}

namespace ECS {
    class Editor;
    struct Rigid;
    struct ECS;
    struct Snapshot;
}

namespace Effects {
    struct ScatterLaser;
    class System;
}

namespace MeshViz {
    struct Instance;
    class System;
}

namespace NavMesh {
    struct Instance;
    struct RouteRequest;
    class System;
}

namespace Obstacle {
    class FluffSystem;
    struct FluffType;
    struct Obstruction;
    class Renderer;
    class System;
    struct Type;
}

namespace Render {
    struct DepthPass;
    struct MainPass;
}

namespace Rigged {
    struct Mesh;
    struct Animation;
};

namespace Terrain {
    struct Instance;
    struct Rendering;
    struct SkyBox;
    class SkyBoxSystem;
    class System;
}

namespace Water {
    struct Instance;
    class System;
}

namespace StartSequence {
    struct Instance;
    class System;
}

namespace Sprite {
    struct Instance;
    class System;
}

namespace RiggedMesh {
    class Instance;
    class Data;
    class System;
}

namespace Parrot {
    struct Instance;
    class System;
}
