// SPDX-License-Identifier: MIT
#pragma once
#include <memory>
#include <vector>

#include <typed-geometry/tg-lean.hh>

namespace Anim {

struct Easing {
    tg::vec2 mP1;
    tg::vec2 mP2;

    static const Easing easeInOut;
    static const Easing easeInOutFast;

    float ease(float t) const;
};

template<typename T> struct KeyFrameTraits;

template<typename T>
struct KeyFrame {
    float time;
    T content;
};

template<typename T>
struct Animation {
    std::vector<KeyFrame<T>> keyFrames;

    bool linear = true;
    Easing easing;

    template<typename RangeT>
    void insertKeyFrames(RangeT frames) {
        for(auto &&frame : frames) {
            keyFrames.push_back(frame);
        }
    }
    void insertKeyFrames(std::initializer_list<KeyFrame<T>> list) {
        insertKeyFrames<std::initializer_list<KeyFrame<T>>>(list);
    }
    void setEasing(const Easing &easing) {
        this->easing = easing;
        linear = false;
    }

    float runTime() const;
    T operator[](float t) const;
};

template<typename T>
struct Animator {
    std::shared_ptr<Animation<T>> animation;
    double start;
    float speed = 1.f;
    bool loop = false;

    T operator[](double time) const;
    bool finished(double time) const {
        return !loop && time >= start + animation->runTime() / speed;
    }
};

}
