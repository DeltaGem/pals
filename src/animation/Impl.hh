// SPDX-License-Identifier: MIT
#pragma once
#include <algorithm>

#include <typed-geometry/tg.hh>

#include "Animation.hh"
#include "ECS/Misc.hh"

namespace Anim {

template<> struct KeyFrameTraits<float> {
    using type = float;
    using keyframe = KeyFrame<type>;

    static type interpolate(type a, type b, float param) {
        return a + param * (b - a);
    }
};

template<> struct KeyFrameTraits<ECS::Rigid> {
    using type = ECS::Rigid;
    using keyframe = KeyFrame<type>;

    static type interpolate(const type &a, const type &b, float param) {
        return {
            tg::lerp(a.translation, b.translation, param),
            tg::slerp(a.rotation, b.rotation, param)
        };
    }
};

template<typename T>
inline float Animation<T>::runTime() const {
    TG_ASSERT(!keyFrames.empty());
    return keyFrames.back().time;
}

template<typename T>
constexpr bool operator<(float a, const KeyFrame<T> &b) {return a < b.time;}
template<typename T>
constexpr bool operator<(const KeyFrame<T> &a, float b) {return a.time < b;}

template<typename T>
inline T Animation<T>::operator[](float t) const {
    auto nextKey = std::upper_bound(keyFrames.begin(), keyFrames.end(), t);
    if (nextKey == keyFrames.begin()) {
        TG_ASSERT(nextKey != keyFrames.end());
        return nextKey->content;
    }
    auto prevKey = nextKey; --prevKey;
    if (nextKey == keyFrames.end()) {return prevKey->content;}

    auto dt = t - prevKey->time, dk = nextKey->time - prevKey->time;
    TG_ASSERT(dt >= 0 && dk > 0);
    auto param = dt / dk;
    if (!linear) {param = easing.ease(param);}

    return KeyFrameTraits<T>::interpolate(
        prevKey->content, nextKey->content, param
    );
}

template<typename T>
inline T Animator<T>::operator[](double time) const {
    time -= start;
    time *= speed;
    if (loop) {time = std::fmod(time, animation->runTime());}
    return (*animation)[float(time)];
}

}
