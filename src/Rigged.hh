// SPDX-License-Identifier: MIT
#pragma once
#include <string>
#include <unordered_map>
#include <vector>

#include <typed-geometry/tg-lean.hh>
#include <glow/fwd.hh>
#include <assimp/mesh.h>

#include <fwd.hh>

struct aiAnimation;
struct aiScene;

namespace Rigged {

using boneid = std::uint8_t;
using BoneMap = std::unordered_map<std::string, boneid>;
using BoneHierarchy = std::vector<std::pair<boneid, boneid>>;

struct Mesh {
    glow::SharedVertexArray vao;
    std::vector<ECS::Rigid> invBindXForm;
};

Mesh loadMesh(const aiMesh *mesh, const BoneMap &boneMap);
BoneHierarchy boneHierarchy(const aiScene *, const BoneMap &boneMap);

struct Animation {
    std::size_t nBones;
    std::vector<ECS::Rigid> pos;
    std::vector<float> times;

    void load(aiAnimation *anim, const BoneMap &boneMap);
    void interpolate(float time, std::vector<ECS::Rigid> &out) const;
    float runTime() const {
        if (times.empty()) {return 0.f;}
        return times.back();
    }
};

template<typename T> tg::quat toQuat(const T &x) {
    return {x.x, x.y, x.z, x.w};
}

}

template<> struct tg::detail::comp_size<aiVector3D> {static constexpr int value = 3;};
