// SPDX-License-Identifier: MIT
#include "Terrain.hh"
#include <cinttypes>

#include <imgui/imgui.h>

#include <navmesh/NavMesh.hh>
#include <startsequence/StartSequence.hh>

using namespace Terrain;

void System::renderingEditorUI(ECS::entity ent, Instance &terr) {
    auto rend_iter = mECS.terrainRenderings.find(ent);
    if (rend_iter != mECS.terrainRenderings.end()) {
        bool want_rendering = true;
        if (ImGui::Checkbox("Terrain Rendering", &want_rendering) && !want_rendering) {
            mECS.terrainRenderings.erase(rend_iter);
        } else {
            ImGui::TextUnformatted("Placeholder for pure rendering options");
        }
    } else {
        bool want_rendering = false;
        if (ImGui::Checkbox("Rendering", &want_rendering) && want_rendering) {
            mECS.terrainRenderings.emplace(ent, Rendering(terr));
        }
    }
}

void System::editorUI() {
    auto ent = mECS.selectedEntity;
    auto terr_iter = mECS.terrains.find(ent);
    if (terr_iter == mECS.terrains.end()) {return;}
    auto &terr = terr_iter->second;

    ImGui::PushID(this);
    ImGui::TextUnformatted("Placeholder for Terrain options");

    auto terr_wo_iter = mECS.staticRigids.find(ent);
    if (terr_wo_iter != mECS.staticRigids.end())
    {
        if (ImGui::Button("Startsequence"))
        {
            mECS.startSequenceSys->startSequence(terr, terr_wo_iter->second);
        }
    }


    renderingEditorUI(ent, terr);
    mECS.navMeshSys->editorUI();
    ImGui::PopID();
}
