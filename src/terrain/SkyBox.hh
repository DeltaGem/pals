// SPDX-License-Identifier: MIT
#pragma once
#include <memory>

#include <typed-geometry/tg-lean.hh>
#include <glow/fwd.hh>

#include <ECS.hh>
#include <ECS/Misc.hh>

namespace Terrain {

struct SkyBox {
    tg::mat4 skyBoxTransformation;
    tg::color3 sandColor;

    explicit SkyBox(Instance const&);
};

class SkyBoxSystem final {
    glow::SharedVertexArray mVao;

    ECS::ComponentMap<SkyBox>& mSkyBoxes;
    ECS::ComponentMap<Terrain::Instance> &mTerrains;

    glow::SharedProgram mShaderSkyBox;
    glow::SharedTexture2D mLowPolyNormalsTex;
    glow::SharedTexture2D mLowPolyNormals2Tex;


public:
    explicit SkyBoxSystem(ECS::ECS &ecs);

    void renderMain(Render::MainPass const&) const;
};

}
