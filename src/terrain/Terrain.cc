// SPDX-License-Identifier: MIT
#include "Terrain.hh"
#include <polymesh/algorithms/delaunay.hh>
#include <polymesh/algorithms/triangulate.hh>
#include <glow/glow.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <external/SimplexNoise.h>

#include <ColorUtil.h>
#include <MathUtil.hh>
#include <ECS/Join.hh>
#include <ECS/Misc.hh>
#include <rendering/RenderPass.hh>
#include "Water.hh"

using namespace Terrain;

const std::array<Material, Material::NumMaterials> Material::table {{
    {Rock, tg::vec3(27, 35, 52)},
    {Grass, tg::vec3(110, 76, 57)},
    {Sand, tg::vec3(52, 65, 57)}
}};

Instance::Instance(std::mt19937 &rng) {
    this->noiseOffset = std::uniform_real_distribution<float> {0.f, 10000.f}(rng);

    this->noiseScale = 120;
    this->noiseOctaves = 8;
    this->mountainHeight = 10;
    // General noise for creating valleys and hills
    this->landscapeNoise = SimplexNoise(1/ noiseScale, 0.5f, 1.99f, 0.5f);
    // smaller noise four roughing up terrain
    this->roughnessNoise = SimplexNoise(10/ noiseScale, 0.5f, 1.99f, 0.5f);

    this->segmentsAmount = 200;
    this->segmentSize = 4;
    auto radius = .5f * segmentsAmount * segmentSize;
    this->center = tg::pos2(radius, radius);

    mesh = std::make_unique<pm::Mesh>();
    this->posAttr = mesh->vertices().make_attribute<tg::pos3>();
    this->colorAttr = mesh->vertices().make_attribute<tg::color3>();

    std::uniform_real_distribution<float> tint {-.12f, std::nextafter(.12f, std::numeric_limits<float>::max())};
    for (uint32_t x = 0; x < segmentsAmount; x++) {
        for (uint32_t z = 0; z < segmentsAmount; z++) {
            auto v = mesh->vertices().add();
            auto pos = posAttr[v] = getVertexPositionForSegment(x, z);
            auto material = getMaterialForPosition(pos);
            auto hsv = Material::table[material].hsv;
            colorAttr[v] = ColorUtil::hsv(hsv.x, hsv.y + tint(rng), hsv.z);
        }
    }

    auto vertex_index_at = [&](uint32_t ix, uint32_t iz) {
        // we know in which order we created vertices
        // thus we can compute index directly
        return mesh->vertices()[ix * segmentsAmount + iz];
    };

    // Connect the created vertices to make faces.
    for (uint32_t x = 0; x < segmentsAmount-1; x++) {
        for (uint32_t z = 0; z < segmentsAmount-1; z++) {
            mesh->faces().add(
                vertex_index_at(x + 0, z + 0),
                vertex_index_at(x + 0, z + 1),
                vertex_index_at(x + 1, z + 1),
                vertex_index_at(x + 1, z + 0)
            );
        }
    }

    pm::triangulate_naive(*mesh);
    pm::make_delaunay(*mesh, posAttr);
    mesh->compactify();
}

Rendering::Rendering(const Instance &terr) {
    std::vector<float> heights;
    heights.reserve(terr.segmentsAmount * terr.segmentsAmount);
    for (auto v : terr.mesh->all_vertices()) {
        heights.push_back(terr.posAttr[v].y);
    }

    std::vector<std::uint8_t> colors;
    colors.reserve(terr.mesh->all_faces().size() * 4);
    std::vector<uint32_t> indices;
    indices.reserve(terr.mesh->all_faces().size() * 3);

    for (auto f : terr.mesh->all_faces()) {
        tg::vec3 color {0, 0, 0};
        int n = 0;
        for (auto v : f.vertices()) {
            indices.push_back(v.idx.value);
            color += tg::vec3(terr.colorAttr[v]);
            n += 1;
        }
        color /= n;
        colors.insert(colors.end(), {
            std::uint8_t(color.x * 255), std::uint8_t(color.y * 255),
            std::uint8_t(color.z * 255), 0
        });
    }
    auto buf = glow::ArrayBuffer::create("aHeight", heights);
    this->vao = glow::VertexArray::create(
        buf, glow::ElementArrayBuffer::create(indices)
    );
    this->colorBuf.buf.init().setData(colors);
    this->colorBuf.init(GL_RGBA8, GL_SAMPLER_BUFFER);
}

Material::ID Instance::getMaterialForPosition(tg::pos3 pos) const {
    if(pos.y > mountainHeight * 0.5f) {
        return Material::Rock;
    } else if(pos.y > waterLevel + 3.) {
        return Material::Grass;
    } else {
        return Material::Sand;
    }
}

tg::pos3 Instance::getVertexPositionForSegment(int x, int z) const
{
    float xPos = (float)x * segmentSize;
    float zPos = (float)z * segmentSize;
    float pointElevation = getElevationAtPos(xPos, zPos);

    return tg::pos3((float)xPos, pointElevation, (float)zPos);
}
float Instance::getElevationAtPos(float xPos, float zPos) const
{
    float landscapeElevation = landscapeNoise.fractal(noiseOctaves, (float)xPos + noiseOffset, (float)zPos + noiseOffset) * mountainHeight;
    float roughnessElevation = roughnessNoise.fractal(1, (float)xPos + noiseOffset, (float)zPos + noiseOffset) * 0;
    float noiseResult = landscapeElevation + roughnessElevation;

    float islandFallOff = getIslandFalloff(xPos, zPos);

    float elevation = noiseResult - islandFallOff;

    float borderElevation = -6.0f;

    float terrainRadius = segmentSize * segmentsAmount / 2;
    float minDistanceFromBorder = tg::min(terrainRadius - std::abs(xPos - terrainRadius), terrainRadius - std::abs(zPos - terrainRadius));

    float flatten = 1- tg::smoothstep(0.0f, 50.0f, minDistanceFromBorder);
    elevation = tg::lerp(elevation, -waterdepth, flatten);

    return elevation;
}
float Instance::getIslandFalloff(float xPos, float zPos) const {
    // Radius is half the size so every water has land below it.
    float islandRadius = (((float)segmentsAmount * segmentSize) / 2) * 0.5f;
    float falloff = (-1 * tg::clamp(islandRadius - tg::distance(tg::pos2(xPos, zPos), center), -waterdepth, 0) * beachSteepness);
    return falloff;
}

System::System(ECS::ECS &ecs) : mECS{ecs} {
    auto vsh = glow::Shader::createFromFile(
        GL_VERTEX_SHADER, "../data/shaders/terrain.vsh"
    );
    mShaderDepth = glow::Program::create(vsh);
    mShader = glow::Program::create({vsh, glow::Shader::createFromFile(
        GL_FRAGMENT_SHADER, "../data/shaders/terrain.fsh"
    )});
}

void System::renderDepth(Render::DepthPass const&pass) const {
    auto shader = mShaderDepth->use();
    pass.applyCommons(shader);
    shader["uMinAlpha"] = 1.f;

    auto range = ECS::Join(pass.snap->rigids, mECS.terrains, mECS.terrainRenderings);
    for (auto &&tup : range) {
        auto &[wo, terr, rend, id] = tup;
        shader["uModel"] = wo.transform_mat();
        shader["uRows"] = terr.segmentsAmount;
        shader["uSegmentSize"] = terr.segmentSize;
        shader["uTerrainRadius"] = (terr.segmentSize * terr.segmentsAmount) / 2;
        rend.vao->bind().draw();
    }
}

void System::renderMain(Render::MainPass const&pass, const float minAlpha) const {
    mShader->setUniformBuffer("uLighting", pass.lightingUniforms);
    auto shader = mShader->use();
    pass.applyCommons(shader);
    shader["uMinAlpha"] = minAlpha;

    auto range = ECS::Join(pass.snap->rigids, mECS.terrains, mECS.terrainRenderings);
    for (auto &&tup : range) {
        auto &[wo, terr, rend, id] = tup;
        shader["uModel"] = wo.transform_mat();
        shader["uRows"] = terr.segmentsAmount;
        shader["uSegmentSize"] = terr.segmentSize;
        shader["uTerrainRadius"] = (terr.segmentSize * terr.segmentsAmount) / 2;
        auto texbuf = rend.colorBuf.bind(shader, "uFaceColors");
        rend.vao->bind().draw();
    }
}
