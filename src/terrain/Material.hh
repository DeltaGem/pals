// SPDX-License-Identifier: MIT
#pragma once
#include <array>
#include <cstdint>
#include <typed-geometry/tg-lean.hh>

namespace Terrain {

struct Material {
    enum ID : short {
        Rock = 0,
        Grass,
        Sand,
        NumMaterials
    };
    ID id;
    tg::vec3 hsv;

    static const std::array<Material, NumMaterials> table;
};

}
