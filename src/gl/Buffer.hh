// SPDX-License-Identifier: MIT
#pragma once
#include <cstdint>
#include <vector>

#include <glad/glad.h>
#include <typed-geometry/feature/assert.hh>
#include <glow/objects/Program.hh>

namespace SimpleGL {

#define SimpleGL_COPY(name, mode) name(name const&) = mode; name& operator=(name const&) = mode
#define SimpleGL_MOVE(name, mode) name(name &&) = mode; name& operator=(name &&) = mode

static constexpr bool VALIDATION = true;

template<typename Tag> struct BufferTraits;
template<typename Tag> struct TextureTraits;

struct TexBuf;
template<> struct BufferTraits<TexBuf> {
    static constexpr GLenum target = GL_TEXTURE_BUFFER, binding = GL_TEXTURE_BUFFER_BINDING;
};
template<> struct TextureTraits<TexBuf> {
    static constexpr GLenum target = GL_TEXTURE_BUFFER, binding = GL_TEXTURE_BINDING_BUFFER;
};

struct ArrayBuf;
template<> struct BufferTraits<ArrayBuf> {
    static constexpr GLenum target = GL_ARRAY_BUFFER, binding = GL_ARRAY_BUFFER_BINDING;
};

template<typename Binding>
struct BoundBuffer {
    GLuint name;

    explicit BoundBuffer(GLuint name) : name(name) {
        if (VALIDATION) {
            GLint prev;
            glGetIntegerv(GL_TEXTURE_BUFFER_BINDING, &prev);
            TG_ASSERT(prev == 0);
        }
        glBindBuffer(BufferTraits<Binding>::target, name);
    }
    SimpleGL_COPY(BoundBuffer, delete);
    BoundBuffer(BoundBuffer &&other) {
        name = other.name;
        other.name = 0;
    }
    BoundBuffer& operator=(BoundBuffer &&other) {
        TG_ASSERT(name == 0);
        name = other.name;
        other.name = 0;
        return *this;
    }
    ~BoundBuffer() {
        if (name == 0) {return;}
        glBindBuffer(BufferTraits<Binding>::target, 0);
        name = 0;
    }

    void setData(const void *data, std::size_t size, GLenum usage = GL_STATIC_DRAW) {
        glBufferData(BufferTraits<Binding>::target, GLsizeiptr(size), data, usage);
    }
    template<typename T>
    void setData(std::vector<T> const& data, GLenum usage = GL_STATIC_DRAW) {
        setData(data.data(), data.size() * sizeof(T), usage);
    }
};

template<typename Binding>
struct Buffer {
    GLuint name = 0;

    Buffer() = default;
    SimpleGL_COPY(Buffer, delete);
    Buffer(Buffer &&other) {
        name = other.name;
        other.name = 0;
    }
    Buffer& operator=(Buffer &&other) {
        TG_ASSERT(name == 0);
        name = other.name;
        other.name = 0;
        return *this;
    }
    ~Buffer() {if (name) {glDeleteBuffers(1, &name);}}

    // not [[nodiscard]], since the caller may in fact not want to do anything with the buffer while still bound
    BoundBuffer<Binding> init() {
        TG_ASSERT(name == 0);
        glGenBuffers(1, &name);
        TG_ASSERT(name != 0);
        return bind();
    }
    [[nodiscard]] BoundBuffer<Binding> bind() {return BoundBuffer<Binding>(name);}
};

template<typename Binding>
struct BoundTexture {
    GLuint name; GLenum unit;

    explicit BoundTexture(GLuint name, GLenum unit) : name(name), unit(unit) {
        glActiveTexture(unit);
        if (VALIDATION) {
            GLint prev;
            glGetIntegerv(TextureTraits<Binding>::target, &prev);
            TG_ASSERT(prev == 0);
        }
        glBindTexture(TextureTraits<Binding>::target, name);
        glBindSampler(unit - GL_TEXTURE0, 0);
    }
    SimpleGL_COPY(BoundTexture, delete);
    BoundTexture(BoundTexture &&other) {
        name = 0;
        *this = std::move(other);
    }
    BoundTexture& operator=(BoundTexture &&other) {
        TG_ASSERT(name == 0);
        name = other.name;
        unit = other.unit;
        other.name = 0;
        return *this;
    }
    ~BoundTexture() {
        if (name == 0) {return;}
        glActiveTexture(unit);
        glBindTexture(TextureTraits<Binding>::target, 0);
        name = 0;
    }
};

struct BufferTexture {
    Buffer<TexBuf> buf;
    GLuint tex = 0;
    GLenum internalFormat, uniformType;

    BufferTexture() = default;
    SimpleGL_COPY(BufferTexture, delete);
    BufferTexture(BufferTexture &&other) {
        tex = 0;
        *this = std::move(other);
    }
    BufferTexture& operator=(BufferTexture &&other) {
        TG_ASSERT(tex == 0);
        buf = std::move(other.buf);
        tex = other.tex;
        internalFormat = other.internalFormat;
        uniformType = other.uniformType;
        other.tex = 0;
        return *this;
    }
    ~BufferTexture() {if (tex) {glDeleteTextures(1, &tex);}}

    // not [[nodiscard]], since the caller may in fact not want to do anything with the texture while still bound
    BoundTexture<TexBuf> init(GLenum internalFormat, GLenum uniformType, GLenum unit = GL_TEXTURE0) {
        TG_ASSERT(tex == 0);
        glGenTextures(1, &tex);
        TG_ASSERT(tex != 0);
        this->internalFormat = internalFormat;
        this->uniformType = uniformType;
        auto res = bind(unit);
        glTexBuffer(GL_TEXTURE_BUFFER, internalFormat, buf.name);
        return res;
    }
    [[nodiscard]] BoundTexture<TexBuf> bind(GLenum unit = GL_TEXTURE0) {
        TG_ASSERT(tex != 0);
        return BoundTexture<TexBuf>(tex, unit);
    }
    [[nodiscard]] BoundTexture<TexBuf> bind(glow::UsedProgram &prog, std::string_view name) {
        TG_ASSERT(tex != 0);
        auto texloc = prog.program->mTextureUnitMapping.getOrAddLocation(name);
        auto uniloc = prog.program->useUniformLocationAndVerify(name, 1, uniformType);
        TG_ASSERT(texloc >= 0 && uniloc >= 0);
        auto res = BoundTexture<TexBuf>(tex, GL_TEXTURE0 + texloc);
        glUniform1i(uniloc, texloc);
        return res;
    }
};

}
