#include "Parrot.hh"
#include <glow/common/scoped_gl.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <Game.hh>
#include <Util.hh>
#include <combat/Combat.hh>
#include <ECS/Join.hh>
#include <ECS/Misc.hh>
#include <rendering/RenderPass.hh>

using namespace Parrot;

System::System(Game& game)
    : mGame{game}, mMesh{mGame.mSharedResources.parrot},
    mBoneHier{mGame.mSharedResources.parrotBoneHier}
{
    auto &anims = mGame.mSharedResources.parrotAnimations;
    auto iter = anims.find("StartFlying");
    TG_ASSERT(iter != anims.end());
    mStartAnim = std::move(iter->second);
    iter = anims.find("Circle");
    TG_ASSERT(iter != anims.end());
    mCircleAnim = std::move(iter->second);
    iter = anims.find("Idle");
    TG_ASSERT(iter != anims.end());
    mIdleAnim = std::move(iter->second);
}

void System::update(ECS::Snapshot &snap) {
    for (auto [rig, parrot, id] : ECS::Join(mGame.mECS.riggedRigids, mGame.mECS.parrots)) {
        if(parrot.startled) {continue;}
        auto &pos = rig.translation;
        for (auto &[humEnt, humpos] : snap.humanoids) {
            if (tg::distance(pos, humpos.base.translation) < startleDistance) {
                parrot.startled = true;
                parrot.startTime = snap.worldTime;
                break;
            }
        }
    }
}

void System::prepareRender(ECS::Snapshot &snap) {
    auto &poses = snap.parrotBones;
    auto nBones = mMesh.invBindXForm.size();
    for (auto [rig, parrot, id] : ECS::Join(mGame.mECS.riggedRigids, mGame.mECS.parrots)) {
        auto localTime = snap.worldTime - parrot.startTime;
        if (localTime < 0.) {continue;}
        const Rigged::Animation *anim = &mIdleAnim;
        if (parrot.startled) {
            if (localTime >= mStartAnim.runTime()) {
                localTime -=mStartAnim.runTime();
                anim = &mCircleAnim;
            } else {
                anim = &mStartAnim;
            }
        }
        float time = std::fmod(localTime, anim->runTime());
        anim->interpolate(time, poses);
        snap.parrots.emplace_back(id, rig);
    }
    TG_ASSERT(snap.parrots.size() * (nBones + 1) == poses.size());
    for (auto i : Util::IntRange(poses.size() / nBones)) {
        auto idx = i * (nBones + 1);
        for (auto [parent, child] : mBoneHier) {
            poses[idx + child] = poses[idx + parent] * poses[idx + child];
        }
        for (auto j : Util::IntRange(nBones)) {
            auto &b = poses[idx + j]; b = b * mMesh.invBindXForm[j];
        }
    }
}

void System::renderDepth(Render::DepthPass const&pass) const {
    std::vector<tg::vec3> translations;
    std::vector<tg::vec4> rotations;
    auto sh = mGame.mSharedResources.rigged.depth->use();
    pass.applyCommons(sh);
    auto vao = mMesh.vao->bind();

    auto &parrots = pass.snap->parrots;
    auto &poses = pass.snap->parrotBones;
    auto nBones = mMesh.invBindXForm.size();
    for (auto i : Util::IntRange(parrots.size())) {
        auto &[id, rig] = parrots[i];
        sh["uModel"] = tg::mat4x3(rig);
        translations.clear(); rotations.clear();
        for (auto j : Util::IntRange(nBones)) {
            auto bone = poses[i * (nBones + 1) + j];
            translations.push_back(tg::vec3(bone.translation));
            rotations.push_back(tg::vec4(bone.rotation));
        }
        sh["uTranslations"] = translations;
        sh["uRotations"] = rotations;
        vao.draw();
    }
}
void System::renderMain(Render::MainPass const&pass) const {
    std::vector<tg::vec3> translations;
    std::vector<tg::vec4> rotations;
    auto sh = mGame.mSharedResources.rigged.main->use();
    pass.applyCommons(sh);
    sh["uTexAlbedo"] = mGame.mSharedResources.colorPaletteTex;
    auto vao = mMesh.vao->bind();

    auto &parrots = pass.snap->parrots;
    auto &poses = pass.snap->parrotBones;
    auto nBones = mMesh.invBindXForm.size();
    for (auto i : Util::IntRange(parrots.size())) {
        auto &[id, rig] = parrots[i];
        sh["uModel"] = tg::mat4x3(rig);
        translations.clear(); rotations.clear();
        for (auto j : Util::IntRange(nBones)) {
            auto bone = poses[i * (nBones + 1) + j];
            translations.push_back(tg::vec3(bone.translation));
            rotations.push_back(tg::vec4(bone.rotation));
        }
        sh["uTranslations"] = translations;
        sh["uRotations"] = rotations;
        vao.draw();
    }
}
