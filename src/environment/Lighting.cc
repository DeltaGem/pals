// SPDX-License-Identifier: MIT
#include "Lighting.hh"
#include <imgui/imgui.h>

using namespace Lighting;


Uniforms Settings::getUniforms(const tg::mat4 &sunViewProj) const {
    return {
        tg::vec4(sunDirection),
        tg::vec4(sunColor * sunIntensity),
        tg::vec4(ambientColor * ambientIntensity),
        sunViewProj,
        tg::vec4(pointLight, Uniforms::lightRadiusTerm(pointLightRange)),
        tg::vec4(pointLightRadiance)
    };
}

void Settings::onGui() {
    if (ImGui::TreeNodeEx("Lighting", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::ColorEdit3("Sun Color", &sunColor.r);
        ImGui::SliderFloat("Sun Intensity", &sunIntensity, 0, 1);
        ImGui::ColorEdit3("Ambient Color", &ambientColor.r);
        ImGui::SliderFloat("Ambient Intensity", &ambientIntensity, 0, 1);
        ImGui::TreePop();
    }
}
