// SPDX-License-Identifier: MIT
#pragma once
#include <glow/fwd.hh>

#include <fwd.hh>
#include <Rigged.hh>

namespace Parrot {

struct Instance {
    bool startled = false;
    double startTime = 0.;
};

class System {
    float startleDistance = 10.f;

    Game& mGame;
    const Rigged::Mesh &mMesh;
    const Rigged::BoneHierarchy &mBoneHier;
    Rigged::Animation mIdleAnim, mStartAnim, mCircleAnim;
public:
    System(Game& game);
    void update(ECS::Snapshot &snap);
    void prepareRender(ECS::Snapshot &snap);
    void renderDepth(Render::DepthPass const&pass) const;
    void renderMain(Render::MainPass const&pass) const;
};
};
