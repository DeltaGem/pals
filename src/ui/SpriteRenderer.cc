// SPDX-License-Identifier: MIT
#include "SpriteRenderer.hh"
#include <typed-geometry/tg-std.hh>
#include <glow/objects.hh>

#include <Game.hh>
#include <MathUtil.hh>
#include <ECS/Join.hh>

using namespace Sprite;

System::System(Game& game) : mGame(game) {
    mShader = glow::Program::createFromFile("../data/shaders/ui/screen_sprite");
    mQuadVao = mGame.mSharedResources.spriteQuad;
    mTexBlack = glow::Texture2D::createFromFile("../data/textures/black.png", glow::ColorSpace::sRGB);
}

void System::resize(tg::isize2 size) {
    mWindowSize = size;
}

void System::render(ECS::Snapshot const&snap) const {
    auto shader = mShader->use();
    auto vao = mQuadVao->bind();
    auto sizeFactor = 2.f / tg::size2(mWindowSize);

    for (auto &sprite : snap.sprites) {
        auto const size = tg::size2(sprite.image->getSize()) * sprite.scale * sizeFactor;
        shader["uTransform"] = tg::mat3x2(
            tg::vec2(size.width, 0.f),
            tg::vec2(0.f, size.height),
            tg::vec2(sprite.pos - sprite.anchor * size)
        );
        shader["uImage"] = sprite.image;
        shader["uAlpha"] = sprite.alpha;

        vao.draw();
    }
}
