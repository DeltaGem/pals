// SPDX-License-Identifier: MIT
#pragma once
#include <glow/fwd.hh>

#include <ECS.hh>

namespace Sprite {

struct Instance {
    glow::SharedTexture2D image;
    tg::vec2 anchor;
    tg::pos2 pos;
    float alpha = 1.f, scale = 1.f;
};

class System {
    Game &mGame;

    glow::SharedVertexArray mQuadVao;
    glow::SharedProgram mShader;
    glow::SharedTexture2D mTexBlack;

    tg::isize2 mWindowSize;

public:
    System(Game&);

    void resize(tg::isize2);
    void render(ECS::Snapshot const&) const;
};

}
