// SPDX-License-Identifier: MIT
#include "StartSequence.hh"
#include <glow/objects.hh>

#include <ECS/Join.hh>
#include <Game.hh>
#include <MathUtil.hh>
#include <Mesh3D.hh>
#include <animation/Impl.hh>
#include <combat/SpawnTool.hh>
#include <rendering/RenderPass.hh>
#include <terrain/Terrain.hh>
#include <ui/SpriteRenderer.hh>

using namespace StartSequence;

template<> struct Anim::KeyFrameTraits<CameraSettings> {
    using type = CameraSettings;
    using keyframe = KeyFrame<type>;

    static type interpolate(const type &a, const type &b, float param) {
        return {
            tg::lerp(a.camPos, b.camPos, param),
            tg::lerp(a.lookAtObject, b.lookAtObject, param)
        };
    }
};

System::System(Game& game) : mGame{game}
{
    auto vsh = glow::Shader::createFromFile(
        GL_VERTEX_SHADER, "../data/shaders/startsequence/spaceship.vsh"
    );
    mDropShipShaderDepth = glow::Program::create(vsh);
    mDropShipShader = glow::Program::create({vsh, glow::Shader::createFromFile(
        GL_FRAGMENT_SHADER, "../data/shaders/startsequence/spaceship.fsh"
    )});
    mGlowShader = glow::Program::createFromFiles({"../data/shaders/startsequence/glow_sphere.fsh", "../data/shaders/startsequence/glow_sphere.vsh"});
    mTexAlbedo = game.mSharedResources.colorPaletteTex;

    mTexLogo = game.mSharedResources.logo;
    mTexTimeAndPlace = glow::Texture2D::createFromFile("../data/textures/intro_time_place.png", glow::ColorSpace::Linear);

    Mesh3D dropshipMesh;
    dropshipMesh.loadFromFile("../data/meshes/dropship.obj", true, false);
    mDropShipVao = dropshipMesh.createVertexArray();

    Mesh3D backThrusters;
    backThrusters.loadFromFile("../data/meshes/dropship_backthrusters.obj", true, false);
    mBackThrustersVao = backThrusters.createVertexArray();

    Mesh3D bottomThrusters;
    bottomThrusters.loadFromFile("../data/meshes/dropship_bottomthrusters.obj", true, false);
    mBottomThrustersVao = bottomThrusters.createVertexArray();

    auto &anims = game.mSharedResources.parrotAnimations;
    auto iter = anims.find("StartSequence");
    TG_ASSERT(iter != anims.end());
    mParrotAnim = &iter->second;

    mBottomThrusterAnim.setEasing(Anim::Easing::easeInOut);
    mCamAnim.setEasing(Anim::Easing::easeInOutFast);
    mSettingAlphaAnim.setEasing(Anim::Easing::easeInOut);
    mLogoAlphaAnim.setEasing(Anim::Easing::easeInOut);
    mTransitionAnim.setEasing(Anim::Easing::easeInOut);
}

void System::startSequence(Terrain::Instance &terrain, const ECS::Rigid &terrainRigid) {
    terrainMat = terrainRigid.transform_mat();
    spawnPosition = tg::pos3(280.0, 0.0, 280.0);
    float spawnElevation = terrain.getElevationAtPos(280, 280);
    spawnPosition.y = spawnElevation;
    tg::pos3 startPos = terrainRigid  * tg::pos3(
        15.0, spawnPosition.y + 50, 15.0
    ), middlePos = terrainRigid * tg::pos3(
        0.8f * spawnPosition.x, spawnPosition.y + 15, 0.8f * spawnPosition.z
    ), endPos = terrainRigid * tg::pos3(
        spawnPosition.x, spawnPosition.y + 10, spawnPosition.z
    );
    auto up = terrainRigid * tg::dir3(0, 1, 0);
    const tg::quat startFlightDirection = Util::lookAtOrientation(
        startPos, middlePos, up
    ), landingFlightDirection = Util::lookAtOrientation(
        middlePos, endPos + tg::pos3(0, 20, 0), up
    );
    mDropShipAnim.insertKeyFrames({
        {0.0f, {startPos, startFlightDirection}},
        {0.6f * mRunTime, {middlePos, startFlightDirection}},
        {mRunTime, {endPos, landingFlightDirection}}
    });
    mBottomThrusterAnim.insertKeyFrames({
        {0.f, 0.f}, {.6f * mRunTime, 0.f}, {.7f * mRunTime, 1.f}
    });

    mGame.mCamera.mControlMode = Camera::ScriptControlled;
    mOwningCamera = true;

    mCamAnim.insertKeyFrames({
        {0.f, {{0, 30, 0}, 0.f}},
        {.1f * mRunTime, {{0, 30, 0}, 0.f}},
        {.25f * mRunTime, {{-5, 5, -30}, 1.f}},
        {.45f * mRunTime, {{-30, -5, 15}, 1.f}},
        {.65f * mRunTime, {{30, 5, 30}, 1.f}},
        {.85f * mRunTime, {{25, 20, 15}, 1.f}},
        {mRunTime, {{5, 5, -5}, 1.f}}
    });

    mSettingAlphaAnim.insertKeyFrames({
        {0.f, 0.f},
        {.1f * mRunTime, 1.f}, {.25f * mRunTime, 1.f},
        {.35f * mRunTime, 0.f}
    });
    mLogoAlphaAnim.insertKeyFrames({
        {0.f, 0.f}, {.31f * mRunTime, 0.f},
        {.51f * mRunTime, 1.f}, {.65f * mRunTime, 1.f},
        {.8f * mRunTime, 0.f}
    });
    mTransitionAnim.insertKeyFrames({
        {0.f, 0.f}, {mRunTime - 6.f, 0.f},
        {mRunTime, 1.f}, {mRunTime + 1.f, 1.f},
        {mRunTime + 4.f, 0.f}
    });

    mStartTime = mGame.mECS.simSnap->worldTime;
}

void System::update(ECS::Snapshot &snap) {
    float localTime = snap.worldTime - mStartTime;

    // the fade effect continues to run after the sequence proper ends
    if (mTransitionAnim.keyFrames.empty()) {return;}
    if (mTransitionAnim.runTime() < localTime) {
        mTransitionAnim.keyFrames.clear();
        mGame.mPostProcess.fade = 1.f;
        return;
    }

    if (!mOwningCamera || localTime < mRunTime) {return;}

    mDropShipAnim.keyFrames.clear();
    mCamAnim.keyFrames.clear();
    for (auto x : {&mLogoAlphaAnim, &mSettingAlphaAnim, &mBottomThrusterAnim}) {
        x->keyFrames.clear();
    }
    spawnPlayerUnits();
    mGame.mCamera.mControlMode = Camera::ControlMode::AbsoluteVertical;
    mOwningCamera = false;
}

struct ParrotInfo {
    tg::pos3 offset;
    float animStartTime;
};
static constexpr std::initializer_list<ParrotInfo> parrotInfo {
    {{3.0, 30.8, 5.0}, 0.17f},
    {{4.0, 31.8, 5.25}, 0.08f},
    {{5.0, 30.5, 5.5}, 0.12f}
};

void System::updateCamera(ECS::Snapshot const& snap, tg::dir3 const& sunDir, tg::dir3 const& up) const {
    float localTime = snap.worldTime - mStartTime;
    ECS::Rigid dropship = mDropShipAnim[localTime];
    CameraSettings settings = mCamAnim[localTime];

    Camera &cam = mGame.mCamera;
    cam.mPos = dropship  * tg::pos3(settings.camPos);
    auto shipVec = dropship.translation - cam.mPos;
    const auto& lookAtShip = Util::forwardUpOrientation(shipVec, up);
    const auto& lookAtSun = Util::forwardUpOrientation(mGame.mLightingSettings.sunDirection, up);
    cam.mOrient = tg::slerp(lookAtSun, lookAtShip, settings.lookAtObject);
    mGame.mPostProcess.mFocusDistance = tg::lerp(5.0f, tg::length(shipVec), settings.lookAtObject);
}

void System::prepareRender(ECS::Snapshot &snap) const {
    float localTime = snap.worldTime - mStartTime;

    if (mTransitionAnim.keyFrames.empty()) {return;}
    mGame.mPostProcess.fade = 1.f - mTransitionAnim[localTime];

    if (localTime < 0.f || localTime > mRunTime) {return;}

    snap.sprites.push_back({
        mTexTimeAndPlace, {0.f, 0.f}, {-.95f, -.95f},
        mSettingAlphaAnim[localTime], .5f
    });
    snap.sprites.push_back({
        mTexLogo, {.5f, 1.f}, {0.f, .95f},
        mLogoAlphaAnim[localTime], .5f
    });
    ECS::Rigid dropship = mDropShipAnim[localTime];

    for(auto parrot : parrotInfo) {
        auto parrotTime = localTime - parrot.animStartTime;
        if (parrotTime < 0.f || parrotTime > mParrotAnim->runTime()) {continue;}
        mParrotAnim->interpolate(parrotTime, snap.parrotBones);
        snap.parrots.emplace_back(ECS::INVALID, dropship * ECS::Rigid {parrot.offset});
    }
}

void System::renderDepth(Render::DepthPass const& pass) const {
    float localTime = pass.snap->worldTime - mStartTime;
    if (localTime < 0.f || localTime > mRunTime) {return;}

    ECS::Rigid dropship = mDropShipAnim[localTime];

    auto shader = mDropShipShaderDepth->use();
    pass.applyCommons(shader);

    shader["uModel"] = tg::mat4x3(dropship);
    mDropShipVao->bind().draw();
}

void System::renderMain(Render::MainPass const& pass) const {
    float localTime = pass.snap->worldTime - mStartTime;
    if (localTime < 0.f || localTime > mRunTime) {return;}

    ECS::Rigid dropship = mDropShipAnim[localTime];

    mDropShipShader->setUniformBuffer("uLighting", pass.lightingUniforms);
    auto shader = mDropShipShader->use();
    pass.applyCommons(shader);

    shader["uModel"] = tg::mat4x3(dropship);
    shader["uTexAlbedo"] = mTexAlbedo;

    mDropShipVao->bind().draw();
}

void System::renderTransparent(Render::MainPass const& pass) const {
    float localTime = pass.snap->worldTime - mStartTime;
    if (localTime < 0.f || localTime > mRunTime) {return;}

    ECS::Rigid dropship = mDropShipAnim[localTime];

    auto sh = mGlowShader->use();
    pass.applyCommons(sh);
    pass.applyTime(sh);
    sh["uModel"] = tg::mat4x3(dropship);

    sh["uAlpha"] = 1.0f;
    mBackThrustersVao->bind().draw();

    sh["uAlpha"] = mBottomThrusterAnim[localTime];
    mBottomThrustersVao->bind().draw();
}

void System::spawnPlayerUnits() {
    auto spawnTool = Combat::SpawnTool(mGame.mECS);
    int spawnedUnits = 0;
    float spawnRadius = 2.5;
    auto placeOnCircle = 0.0f;
    while (spawnedUnits < 4)
    {
        auto theta = placeOnCircle * 2.0f * 3.1415926f;
        tg::vec3 randomCircleCoordinate(spawnRadius * cos(theta), 0, spawnRadius * sin(theta));

        const auto rayOriginLocalSpace = spawnPosition + tg::pos3(0, 50, 0) + randomCircleCoordinate;
        const auto rayOriginWorldSpace = tg::pos3(terrainMat * tg::vec4(rayOriginLocalSpace, 1.0));
        const tg::ray3 spawnRay = tg::ray3(rayOriginWorldSpace, tg::dir3::neg_y);
        auto newUnit = spawnTool.spawnUnit(spawnRay);
        if (newUnit.has_value())
        {
            spawnedUnits++;
        }
        placeOnCircle += 0.2f;
        if (placeOnCircle > 1.0f)
        {
            placeOnCircle = 0.0f;
            spawnRadius += 1.5f;
        }
    }
}
