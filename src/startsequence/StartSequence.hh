// SPDX-License-Identifier: MIT
#pragma once
#include <glow/fwd.hh>

#include <fwd.hh>
#include <Rigged.hh>
#include <ECS/Misc.hh>
#include <animation/Animation.hh>

namespace StartSequence {

using Anim::Animation;

struct CameraSettings {
    tg::pos3 camPos;
    float lookAtObject;
};

class System final {
    Game& mGame;
    Rigged::Animation *mParrotAnim;

    bool mOwningCamera = false;
    float mRunTime = 45.0f;
    double mStartTime = -mRunTime;

    glow::SharedProgram mDropShipShader, mDropShipShaderDepth;
    glow::SharedProgram mGlowShader;
    glow::SharedTexture2D mTexAlbedo, mTexLogo, mTexTimeAndPlace;
    glow::SharedVertexArray mDropShipVao, mBackThrustersVao, mBottomThrustersVao;

    ECS::entity logoSprite;
    ECS::entity timeAndPlaceSprite;

    Animation<ECS::Rigid> mDropShipAnim;
    Animation<CameraSettings> mCamAnim;
    Animation<float> mLogoAlphaAnim, mSettingAlphaAnim, mTransitionAnim, mBottomThrusterAnim;

public:
    tg::pos3 spawnPosition;
    tg::mat4x3 terrainMat;
    System(Game&);
    void startSequence(Terrain::Instance&, const ECS::Rigid &terrainRigid);

    void updateCamera(ECS::Snapshot const& snap, tg::dir3 const& sunDir, tg::dir3 const& up) const;
    void prepareRender(ECS::Snapshot &snap) const;
    void renderDepth(Render::DepthPass const&) const;
    void renderMain(Render::MainPass const&) const;
    void renderTransparent(Render::MainPass const&) const;

    void update(ECS::Snapshot &next);

    void stopSequence();
    void spawnPlayerUnits();
};

}


