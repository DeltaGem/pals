// SPDX-License-Identifier: MIT
#pragma once
#include <optional>

#include <polymesh/Mesh.hh>
#include <glow/objects.hh>

#include <ECS.hh>
#include <ECS/Misc.hh>
#include "Collision.hh"
#include "Renderer.hh"
#include <terrain/Material.hh>
#include <util/SparseDiscreteDistribution.hh>

namespace Obstacle {

struct Type {
    int id;
    Renderer::VaoInfo &vaoInfo;
    std::unique_ptr<CollisionMesh> collisionMesh;
    bool highCover;

    Type(Renderer::VaoInfo &vaoInfo) : vaoInfo{vaoInfo} {}
    Type(Type &&) = default;
};

class System final {
    ECS::ECS &mECS;

    float obstacleDensity = 0.1f;
    float parrotDensity = 0.45;

    std::vector<Type> mTypes;
    std::array<SparseDiscreteDistribution<size_t, float>, Terrain::Material::NumMaterials> mTypesForTerrain;

    std::vector<tg::pos3> randomlySelectedObstaclePositions(const Terrain::Instance &terr, std::mt19937 &engine) const;
    void initObstacleCollider(CollisionMesh &collider, const char *meshName) const;

    void spawnParrot(const ECS::Rigid& wo, const tg::quat& randomRotation, const tg::pos3& worldPos, std::mt19937& rng);

public:
    std::map<glow::SharedVertexArray, CollisionMesh> obstacleColliders;

    System(Game&);

    void editorUI();
    void select(tg::ray3 const&, float&) const;

    void spawnObstacles(const ECS::Rigid &wo, const Terrain::Instance &terr, std::mt19937& rng);
    using QueryResult = std::optional<std::pair<ECS::entity, float>>;
    QueryResult rayCast(const tg::ray3 &ray) const;
    QueryResult closest(const tg::pos3 &pos) const;
};
}
