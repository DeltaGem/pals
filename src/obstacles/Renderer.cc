// SPDX-License-Identifier: MIT
#include "Renderer.hh"
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <Game.hh>
#include <Mesh3D.hh>
#include <ECS/Join.hh>
#include <ECS/Misc.hh>
#include "Obstacle.hh"
#include "WorldFluff.hh"
#include <rendering/RenderPass.hh>

using namespace Obstacle;

InstanceData::InstanceData(const ECS::Rigid &rig) {
    auto rot = tg::mat3(tg::conjugate(rig.rotation));
    row0 = tg::vec4(rot[0], rig.translation.x);
    row1 = tg::vec4(rot[1], rig.translation.y);
    row2 = tg::vec4(rot[2], rig.translation.z);
}

glow::SharedArrayBuffer InstanceData::makeABO() {
    auto abo = glow::ArrayBuffer::create();
    for (auto &attr : std::initializer_list<glow::ArrayBufferAttribute> {
        {&InstanceData::row0, "iModel_row0"},
        {&InstanceData::row1, "iModel_row1"},
        {&InstanceData::row2, "iModel_row2"},
    }) {
        abo->defineAttribute(attr);
    }
    abo->setDivisor(1);
    return abo;
}

Renderer::Renderer(Game &game)
: mECS{game.mECS}, mPaletteTex(game.mSharedResources.colorPaletteTex) {
    auto flatFSh = glow::Shader::createFromFile(
        GL_FRAGMENT_SHADER, "../data/shaders/flat/flat.fsh"
    ), flatInstancedVSh = glow::Shader::createFromFile(
        GL_VERTEX_SHADER, "../data/shaders/flat/flat_instanced.vsh"
    ), flatWindyVSh = glow::Shader::createFromFile(
        GL_VERTEX_SHADER, "../data/shaders/flat/flat_windy.vsh"
    );
    mStillShaderDepth = glow::Program::create(flatInstancedVSh);
    mStillShader = glow::Program::create({flatInstancedVSh, flatFSh});
    mWindShaderDepth = glow::Program::create(flatWindyVSh);
    mWindShader = glow::Program::create({flatWindyVSh, flatFSh});
}

void Renderer::render(Render::MainPass const&pass, bool reflectRefract) const {
    {
        mStillShader->setUniformBuffer("uLighting", pass.lightingUniforms);
        auto sh = mStillShader->use();
        pass.applyCommons(sh);
        sh["uTexAlbedo"] = mPaletteTex;

        for(auto &vaoInfo : mVaoInfos) {
            if (reflectRefract && !vaoInfo->renderReflectRefract) {continue;}
            auto &wind = vaoInfo->windSettings;
            if (wind.objectHeight < wind.startFromHeight) {continue;}
            vaoInfo->vao->bind().draw();
        }
    }
    {
        mWindShader->setUniformBuffer("uLighting", pass.lightingUniforms);
        auto sh = mWindShader->use();
        pass.applyCommons(sh);
        pass.applyTime(sh);
        pass.applyWind(sh);
        sh["uTexAlbedo"] = mPaletteTex;

        for(auto &vaoInfo : mVaoInfos) {
            if (reflectRefract && !vaoInfo->renderReflectRefract) {continue;}
            auto &wind = vaoInfo->windSettings;
            if (wind.objectHeight >= wind.startFromHeight) {continue;}
            sh["uObjectHeight"] = wind.objectHeight;
            sh["uWindAffectFrom"] = wind.startFromHeight;
            vaoInfo->vao->bind().draw();
        }
    }
}

void Renderer::renderDepth(Render::DepthPass const&pass) const {
    {
        auto sh = mStillShaderDepth->use();
        pass.applyCommons(sh);

        for(auto &vaoInfo : mVaoInfos) {
            auto &wind = vaoInfo->windSettings;
            if (wind.objectHeight < wind.startFromHeight) {continue;}
            vaoInfo->vao->bind().draw();
        }
    }
    {
        auto sh = mWindShaderDepth->use();
        pass.applyCommons(sh);
        pass.applyTime(sh);
        pass.applyWind(sh);

        for(auto &vaoInfo : mVaoInfos) {
            auto &wind = vaoInfo->windSettings;
            if (wind.objectHeight >= wind.startFromHeight) {continue;}
            sh["uObjectHeight"] = wind.objectHeight;
            sh["uWindAffectFrom"] = wind.startFromHeight;
            vaoInfo->vao->bind().draw();
        }
    }
}
void Renderer::renderMain(Render::MainPass const&pass) const {
    render(pass, false);
}
void Renderer::renderReflectRefract(Render::MainPass const&pass) const {
    render(pass, true);
}

Renderer::VaoInfo& Renderer::loadVao(const std::string& filePath) {
    Mesh3D mesh;
    mesh.loadFromFile(filePath, true,false);
    glow::SharedVertexArray vao = mesh.createVertexArray();
    auto fluffTransformations = InstanceData::makeABO();
    vao->bind().attach(fluffTransformations);

    auto vaoInfo = std::make_unique<VaoInfo>();
    vaoInfo->vao = std::move(vao);
    vaoInfo->instancedDataBuffer = fluffTransformations;
    auto height = mesh.maxExtents.y;
    vaoInfo->windSettings = Wind::PerMeshSettings {height, height};

    return *mVaoInfos.emplace_back(std::move(vaoInfo));
}

void Renderer::updateBuffers() {
    for (auto [rig, type, id] : ECS::Join(mECS.instancedRigids, mECS.obstacles)) {
        type.vaoInfo.instanceData.emplace_back(rig);
    }
    for (auto [rig, type, id] : ECS::Join(mECS.instancedRigids, mECS.worldFluffs)) {
        type.vaoInfo.instanceData.emplace_back(rig);
    }
    for (auto &vaoInfo : mVaoInfos) {
        auto &instanceData = vaoInfo->instanceData;
        vaoInfo->instancedDataBuffer->bind().setData(instanceData);
        instanceData.clear();
    }
}
