// SPDX-License-Identifier: MIT
#pragma once
#include <polymesh/fwd.hh>
#include <glow/fwd.hh>

#include <ECS.hh>
#include <ECS/Misc.hh>
#include "Renderer.hh"
#include <terrain/Material.hh>
#include <util/SparseDiscreteDistribution.hh>

namespace Obstacle {

struct FluffType {
    Renderer::VaoInfo &vaoInfo;

    FluffType(Renderer::VaoInfo &vaoInfo) : vaoInfo{vaoInfo} {}
};

class FluffSystem final {
    ECS::ECS &mECS;

    float fluffDensity = 0.20;

    std::vector<FluffType> mTypes;
    std::array<SparseDiscreteDistribution<size_t, float>, Terrain::Material::NumMaterials> mTypesForTerrain;

    std::vector<polymesh::face_handle> randomlySelectFluffPositions(const Terrain::Instance &terr, std::mt19937 &rng) const;

public:
    FluffSystem(Game&);
    void spawnFluff(const ECS::Rigid &wo, const Terrain::Instance &terr, std::mt19937& rng);
};
}
