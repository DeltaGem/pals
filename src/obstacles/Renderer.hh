// SPDX-License-Identifier: MIT
#pragma once
#include <map>
#include <string>
#include <vector>

#include <glow/fwd.hh>

#include <fwd.hh>
#include <environment/Wind.hh>

namespace Obstacle {

struct InstanceData {
    tg::vec4 row0, row1, row2;

    InstanceData(const ECS::Rigid &rigid);
    static glow::SharedArrayBuffer makeABO();
};

class Renderer {
public:
    struct VaoInfo {
        glow::SharedVertexArray vao;
        glow::SharedArrayBuffer instancedDataBuffer;
        Wind::PerMeshSettings windSettings;
        bool renderReflectRefract = true;
        std::vector<InstanceData> instanceData;
    };

    Renderer(Game &);
    VaoInfo& loadVao(std::string const&filePath);
    void updateBuffers();

    void renderDepth(Render::DepthPass const&) const;
    void renderMain(Render::MainPass const&) const;
    void renderReflectRefract(Render::MainPass const&) const;

private:
    ECS::ECS &mECS;
    glow::SharedTexture2D mPaletteTex;
    glow::SharedProgram mStillShader, mStillShaderDepth;
    glow::SharedProgram mWindShader, mWindShaderDepth;
    std::vector<std::unique_ptr<VaoInfo>> mVaoInfos;

    void render(Render::MainPass const&, bool reflectRefract) const;
};

}
