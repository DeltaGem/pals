#pragma once
#include <optional>

#include <typed-geometry/detail/utility.hh>
#include <typed-geometry/feature/assert.hh>

namespace tg
{
template<typename T>
using optional = std::optional<T>;
}
