#include "Importer.hh"

#include <fstream>

#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

#include <glow/common/profiling.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/VertexArray.hh>
#include <map>
#include <typed-geometry/tg.hh>

namespace
{
template <class T = tg::vec3>
T aiCast(aiVector3D const& v)
{
    return {v.x, v.y, v.z};
}

template <class T = tg::quat>
T aiCast(aiQuaternion const& q)
{
    return {q.x, q.y, q.z, q.w};
}

template <class T = tg::vec2>
T aiCast2D(aiVector3D const& v)
{
    return {v.x, v.y};
}

template <class T = tg::vec4>
T aiCast(aiColor4D const& v)
{
    return {v.r, v.g, v.b, v.a};
}

template <class T = tg::mat4>
T aiCast(aiMatrix4x4 const& mat)
{
    tg::mat4 to;
    //the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
    to[0][0] = mat.a1; to[1][0] = mat.a2; to[2][0] = mat.a3; to[3][0] = mat.a4;
    to[0][1] = mat.b1; to[1][1] = mat.b2; to[2][1] = mat.b3; to[3][1] = mat.b4;
    to[0][2] = mat.c1; to[1][2] = mat.c2; to[2][2] = mat.c3; to[3][2] = mat.c4;
    to[0][3] = mat.d1; to[1][3] = mat.d2; to[2][3] = mat.d3; to[3][3] = mat.d4;
    return to;
}

tg::pos3 posMin(tg::pos3 lhs, tg::pos3 rhs)
{
    return {std::min(lhs.x, rhs.x), std::min(lhs.y, rhs.y), std::min(lhs.z, rhs.z)};
}
tg::pos3 posMax(tg::pos3 lhs, tg::pos3 rhs)
{
    return {std::max(lhs.x, rhs.x), std::max(lhs.y, rhs.y), std::max(lhs.z, rhs.z)};
}
}

glow::assimp::Importer::Importer() {}

glow::SharedVertexArray glow::assimp::Importer::load(const std::string& filename) const
{
    GLOW_ACTION();

    auto data = loadData(filename);
    return data ? data->createVertexArray() : nullptr;
}

glow::assimp::SharedMeshData glow::assimp::Importer::loadData(const std::string& filename) const
{
    using namespace assimp;

    if (!std::ifstream(filename).good())
    {
        error() << "Error loading `" << filename << "' with Assimp.";
        error() << "  File not found/not readable";
        return nullptr;
    }

    uint32_t flags = aiProcess_SortByPType;

    if (mTriangulate)
        flags |= aiProcess_Triangulate;
    if (mCalculateTangents)
        flags |= aiProcess_CalcTangentSpace;
    if (mGenerateSmoothNormal)
        flags |= aiProcess_GenSmoothNormals;
    if (mGenerateUVCoords)
        flags |= aiProcess_GenUVCoords;
    if (mPreTransformVertices && !mLoadBones)
        flags |= aiProcess_PreTransformVertices;
    if (mFlipUVs)
        flags |= aiProcess_FlipUVs;
    if(mLoadBones)
        flags |= aiProcess_Debone;


    Assimp::Importer importer;
    if (mLoadBones)
    {
        // Always have a maximum of 4 bones affecting one vertex
        importer.SetPropertyFloat(AI_CONFIG_PP_DB_THRESHOLD, 4.0f);
    }

    auto scene = importer.ReadFile(filename, flags);

    if (!scene)
    {
        error() << "Error loading `" << filename << "' with Assimp.";
        error() << "  " << importer.GetErrorString();
        return nullptr;
    }

    if (!scene->HasMeshes())
    {
        error() << "File `" << filename << "' has no meshes.";
        return nullptr;
    }

    auto data = std::make_shared<MeshData>();

    auto& positions = data->positions;
    auto& normals = data->normals;
    auto& tangents = data->tangents;
    auto& texCoords = data->texCoords;
    auto& colors = data->colors;
    auto& indices = data->indices;
    auto& boneWeights = data->boneWeights;
    auto& boneIds = data->boneIds;

    auto baseIdx = 0u;
    for (auto i = 0u; i < scene->mNumMeshes; ++i)
    {
        auto const& mesh = scene->mMeshes[i];
        auto colorsCnt = mesh->GetNumColorChannels();
        auto texCoordsCnt = mesh->GetNumUVChannels();

        if (texCoords.empty())
            texCoords.resize(texCoordsCnt);
        else if (texCoords.size() != texCoordsCnt)
        {
            error() << "File `" << filename << "':";
            error() << "  contains inconsistent texture coordinate counts";
            return nullptr;
        }

        if (colors.empty())
            colors.resize(colorsCnt);
        else if (colors.size() != colorsCnt)
        {
            error() << "File `" << filename << "':";
            error() << "  contains inconsistent vertex color counts";
            return nullptr;
        }

        // add faces
        auto fCnt = mesh->mNumFaces;
        for (auto f = 0u; f < fCnt; ++f)
        {
            auto const& face = mesh->mFaces[f];
            if (face.mNumIndices != 3)
            {
                error() << "File `" << filename << "':.";
                error() << "  non-3 faces not implemented/supported";
                return nullptr;
            }

            for (auto fi = 0u; fi < face.mNumIndices; ++fi)
            {
                indices.push_back(baseIdx + face.mIndices[fi]);
            }
        }

        data->aabbMin = tg::pos3(+std::numeric_limits<float>().infinity());
        data->aabbMax = tg::pos3(-std::numeric_limits<float>().infinity());

        // add vertices
        auto vCnt = mesh->mNumVertices;
        for (auto v = 0u; v < vCnt; ++v)
        {
            auto const vertexPosition = aiCast<tg::pos3>(mesh->mVertices[v]);

            data->aabbMin = posMin(data->aabbMin, vertexPosition);
            data->aabbMax = posMax(data->aabbMax, vertexPosition);

            positions.push_back(vertexPosition);

            if (mLoadBones)
            {
                boneIds.push_back(tg::ivec4(-1, -1, -1, -1));
            }

            if (mesh->HasNormals())
                normals.push_back(aiCast(mesh->mNormals[v]));
            if (mesh->HasTangentsAndBitangents())
                tangents.push_back(aiCast(mesh->mTangents[v]));

            for (auto t = 0u; t < texCoordsCnt; ++t)
                texCoords[t].push_back(aiCast2D<tg::pos2>(mesh->mTextureCoords[t][v]));
            for (auto t = 0u; t < colorsCnt; ++t)
                colors[t].push_back(aiCast<tg::color4>(mesh->mColors[t][v]));
        }

        if (mesh->HasBones() && mLoadBones)
        {
            data->objectRootTransform = tg::inverse(aiCast(scene->mRootNode->mChildren[0]->mTransformation));
            boneWeights.resize(mesh->mNumVertices);
            std::vector<int> vertexCurrentBone(mesh->mNumVertices);
            auto bCnt = mesh->mNumBones;
            for (auto b = 0u; b < bCnt; ++b)
            {
                auto const bone = mesh->mBones[b];
                auto boneNode = scene->mRootNode->FindNode(bone->mName);
                std::string parent = boneNode->mParent->mName.data;
                std::vector<std::string> children;
                for(auto c = 0u; c < boneNode->mNumChildren; ++c) {
                    children.emplace_back(boneNode->mChildren[c]->mName.data);
                }

                std::string boneName = bone->mName.data;
                if (boneNode->mParent->mParent->mParent == NULL)
                {
                    // Parent: Object | Parent->Parent == Scene, Parent->Parent-Parent == NULL, then we have to root bone of the armature.
                    // (Not a good way of finding out, but I don't see another way)
                    data->rootBoneName = boneName;
                }

                data->bones[boneName] = {
                    b,
                    parent,
                    children,
                    aiCast(bone->mOffsetMatrix)
                };

                auto wCnt = bone->mNumWeights;
                for (auto w = 0u; w < wCnt; ++w)
                {
                    auto weight = bone->mWeights[w];
                    auto forVertex = weight.mVertexId;
                    auto currentBone = vertexCurrentBone[forVertex];
                    boneWeights[forVertex][currentBone] = weight.mWeight;
                    boneIds[forVertex][currentBone] = b;
                    vertexCurrentBone[forVertex]++;
                }
            }
        }

        baseIdx = static_cast<unsigned>(positions.size());
    }

    auto animCnt = scene->mNumAnimations;
    for (auto a = 0u; a < animCnt; ++a) {
        auto animation = scene->mAnimations[a];
        AnimationData animData;
        animData.mDuration = animation->mDuration;
        animData.mTicksPerSecond = animation->mTicksPerSecond;

        auto channelsCnt = animation->mNumChannels;
        animData.boneAnims = std::vector<BoneAnim>(data->bones.size());
        for (auto c = 0u; c < channelsCnt; ++c)
        {
            auto channel = animation->mChannels[c];
            auto forBone = data->bones.find(channel->mNodeName.data);
            if (forBone == data->bones.end()) {
                // Skip animations not for bones, since we can't handle them.
                continue;
            }
            BoneAnim boneAnim;
            // one channel of keyframes per bone
            boneAnim.boneName = channel->mNodeName.data;
            auto keyFramesCount = channel->mNumPositionKeys;
            for (auto p = 0u; p < keyFramesCount; ++p)
            {
                auto posFrame = channel->mPositionKeys[p];
                BoneFrame<tg::pos3> frame = {
                    posFrame.mTime,
                    aiCast<tg::pos3>(posFrame.mValue)
                };
                boneAnim.positionFrames.emplace_back(frame);
            }
            for (auto r = 0u; r < keyFramesCount; ++r)
            {
                auto rotFrame = channel->mRotationKeys[r];
                BoneFrame<tg::quat> frame = {
                    rotFrame.mTime,
                    aiCast(rotFrame.mValue)
                };
                boneAnim.rotationFrames.emplace_back(frame);
            }
            animData.boneAnims[forBone->second.id] = boneAnim;
        }

        data->animations[mLoadAnimationName] = animData;
    }

    return data;
}
