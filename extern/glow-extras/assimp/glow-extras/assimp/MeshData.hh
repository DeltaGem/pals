#pragma once

#include <string>
#include <vector>

#include <typed-geometry/tg-lean.hh>

#include <glow/common/shared.hh>
#include <glow/fwd.hh>
#include <map>

namespace glow
{
namespace assimp
{
GLOW_SHARED(struct, MeshData);

template<typename T>
struct BoneFrame {
    double time;
    T value;
};

struct BoneAnim {
    std::string boneName;
    std::vector<BoneFrame<tg::pos3>> positionFrames;
    std::vector<BoneFrame<tg::quat>> rotationFrames;
};

struct AnimationData {
    int frames;
    double mDuration;
    double mTicksPerSecond;
    std::vector<BoneAnim> boneAnims;
};

struct BoneData {
    unsigned int id;
    std::string parentBone;
    std::vector<std::string> childBones;
    tg::mat4 offsetMat;
};

struct MeshData
{
    std::string filename;

    tg::pos3 aabbMin;
    tg::pos3 aabbMax;

    std::vector<tg::pos3> positions;
    std::vector<tg::vec3> normals;
    std::vector<tg::vec3> tangents;
    std::vector<tg::ivec4> boneIds;
    std::vector<tg::vec4> boneWeights;

    tg::mat4 objectRootTransform;
    std::string rootBoneName;
    std::map<std::string, BoneData> bones;

    // one vector per channel
    std::vector<std::vector<tg::pos2>> texCoords;
    std::vector<std::vector<tg::color4>> colors;

    std::vector<uint32_t> indices;

    std::map<std::string, AnimationData> animations;

    SharedVertexArray createVertexArray() const;
};
}
}
